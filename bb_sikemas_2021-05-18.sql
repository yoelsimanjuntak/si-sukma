# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.36-MariaDB)
# Database: bb_sikemas
# Generation Time: 2021-05-18 13:01:35 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table _postcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postcategories`;

CREATE TABLE `_postcategories` (
  `PostCategoryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryName` varchar(50) NOT NULL,
  `PostCategoryLabel` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`PostCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_postcategories` WRITE;
/*!40000 ALTER TABLE `_postcategories` DISABLE KEYS */;

INSERT INTO `_postcategories` (`PostCategoryID`, `PostCategoryName`, `PostCategoryLabel`)
VALUES
	(1,'Inspirasi','#f56954'),
	(5,'Lainnya','#3c8dbc');

/*!40000 ALTER TABLE `_postcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _postimages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postimages`;

CREATE TABLE `_postimages` (
  `PostImageID` bigint(10) NOT NULL AUTO_INCREMENT,
  `PostID` bigint(10) NOT NULL,
  `FileName` varchar(250) NOT NULL,
  `Description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`PostImageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table _posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_posts`;

CREATE TABLE `_posts` (
  `PostID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryID` int(10) NOT NULL,
  `PostDate` date NOT NULL,
  `PostTitle` varchar(200) NOT NULL,
  `PostSlug` varchar(200) NOT NULL,
  `PostContent` longtext NOT NULL,
  `PostExpiredDate` date NOT NULL,
  `TotalView` int(11) NOT NULL DEFAULT '0',
  `LastViewDate` datetime DEFAULT NULL,
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '1',
  `FileName` varchar(250) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  PRIMARY KEY (`PostID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table _roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_roles`;

CREATE TABLE `_roles` (
  `RoleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(50) NOT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_roles` WRITE;
/*!40000 ALTER TABLE `_roles` DISABLE KEYS */;

INSERT INTO `_roles` (`RoleID`, `RoleName`)
VALUES
	(1,'Administrator'),
	(2,'Operator');

/*!40000 ALTER TABLE `_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_settings`;

CREATE TABLE `_settings` (
  `SettingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`SettingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_settings` WRITE;
/*!40000 ALTER TABLE `_settings` DISABLE KEYS */;

INSERT INTO `_settings` (`SettingID`, `SettingLabel`, `SettingName`, `SettingValue`)
VALUES
	(1,'SETTING_WEB_NAME','SETTING_WEB_NAME','SI SUKMA'),
	(2,'SETTING_WEB_DESC','SETTING_WEB_DESC','Sistem Informasi Survei Kepuasan Masyarakat'),
	(3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','https://general-9.disqus.com/embed.js'),
	(4,'SETTING_ORG_NAME','SETTING_ORG_NAME','Bagian Organisasi Setda Kab. Batu Bara'),
	(5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','-'),
	(6,'SETTING_ORG_LAT','SETTING_ORG_LAT',''),
	(7,'SETTING_ORG_LONG','SETTING_ORG_LONG',''),
	(8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','-'),
	(9,'SETTING_ORG_FAX','SETTING_ORG_FAX','-'),
	(10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','-'),
	(11,'SETTING_WEB_API_FOOTERLINK','SETTING_WEB_API_FOOTERLINK','-'),
	(12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','logo.png'),
	(13,'SETTING_WEB_SKIN_CLASS','SETTING_WEB_SKIN_CLASS','skin-green-light'),
	(14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','loader.gif'),
	(15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.0'),
	(16,'SETTING_ORG_LINKFACEBOOK','SETTING_ORG_LINKFACEBOOK','https://www.facebook.com/Mentor-Huis-105230401189706'),
	(17,'SETTING_ORG_LINKTWITTER','SETTING_ORG_LINKTWITTER','https://twitter.com/MentorhuisID'),
	(18,'SETTING_ORG_LINKINSTAGRAM','SETTING_ORG_LINKINSTAGRAM',''),
	(19,'SETTING_WEB_WELCOMEMSG','SETTING_WEB_WELCOMEMSG','<p style=\"text-align: justify;\"><strong>SI SUKMA</strong> merupakan platform Survei Kepuasan Masyarakat yang membantu kegiatan pengukuran secara komprehensif tentang tingkat kepuasan masyarakat terhadap kualitas layanan yang diberikan oleh penyelenggara pelayanan publik dengan tujuan untuk mengetahui kelemahan atau kekurangan dari masing-masing unsur dalam penyelenggara pelayanan publik secara periodik.</p>\n\n<p style=\"text-align: justify;\">Sebagaimana diamanatkan dalam <strong>Peraturan Menteri PAN RB Nomor 14 Tahun 2017</strong>&nbsp;tentang Pedoman Penyusunan Survei Kepuasan Masyarakat Unit Penyelenggara Pelayanan Publik, Pemerintah Kabupaten Batu Bara sebagai penyelenggara pelayanan publik melaksanakan survei&nbsp;untuk memperoleh Indeks Kepuasan Masyarakat secara terpadu. SI SUKMA hadir agar Survei Kepuasan Masyarakat dapat disajikan secara online sehingga lebih mudah diakses oleh masyarakat selaku penerima layanan.</p>');

/*!40000 ALTER TABLE `_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _userinformation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_userinformation`;

CREATE TABLE `_userinformation` (
  `UserName` varchar(50) NOT NULL,
  `IdUnit` bigint(10) DEFAULT NULL,
  `Email` varchar(50) NOT NULL DEFAULT '',
  `NM_FullName` varchar(50) DEFAULT NULL,
  `NM_ProfileImage` varchar(250) DEFAULT NULL,
  `DATE_Registered` datetime DEFAULT NULL,
  `IS_EmailVerified` tinyint(1) DEFAULT NULL,
  `IS_LoginViaGoogle` tinyint(1) DEFAULT NULL,
  `IS_LoginViaFacebook` tinyint(1) DEFAULT NULL,
  `NM_GoogleOAuthID` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`UserName`),
  CONSTRAINT `FK_Userinformation_User` FOREIGN KEY (`UserName`) REFERENCES `_users` (`UserName`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_userinformation` WRITE;
/*!40000 ALTER TABLE `_userinformation` DISABLE KEYS */;

INSERT INTO `_userinformation` (`UserName`, `IdUnit`, `Email`, `NM_FullName`, `NM_ProfileImage`, `DATE_Registered`, `IS_EmailVerified`, `IS_LoginViaGoogle`, `IS_LoginViaFacebook`, `NM_GoogleOAuthID`)
VALUES
	('admin',NULL,'admin@bagianorganisasi.batubarakab.go.id','Admin',NULL,'2018-08-17 00:00:00',NULL,NULL,NULL,NULL),
	('adpem',91,'adpem','Bagian Administrasi Pembangunan',NULL,NULL,NULL,NULL,NULL,NULL),
	('airputih',69,'airputih','Kecamatan Air Putih',NULL,NULL,NULL,NULL,NULL,NULL),
	('bappeda',47,'bappeda','Badan Perencanaan Pembangunan Daerah',NULL,NULL,NULL,NULL,NULL,NULL),
	('bkd',42,'bkd','Badan Kepegawaian Daerah',NULL,NULL,NULL,NULL,NULL,NULL),
	('bpbd',44,'bpbd','Badan Penanggulangan Bencana Daerah',NULL,NULL,NULL,NULL,NULL,NULL),
	('bpkad',45,'bpkad','Badan Pengelolaan Keuangan dan Asset Daerah',NULL,NULL,NULL,NULL,NULL,NULL),
	('bprd',46,'bprd','Badan Pengelolaan Pajak dan Retribusi Daerah',NULL,NULL,NULL,NULL,NULL,NULL),
	('datuklimapuluh',70,'datuklimapuluh','Kecamatan Datuk Lima Puluh',NULL,NULL,NULL,NULL,NULL,NULL),
	('datuktanahdatar',71,'datuktanahdatar','Kecamatan Datuk Tanah Datar',NULL,NULL,NULL,NULL,NULL,NULL),
	('dinaskebersihan',54,'dinaskebersihan','Dinas Lingkungan Hidup, Kebersihan dan Pertamanan',NULL,NULL,NULL,NULL,NULL,NULL),
	('dinkes',50,'dinkes','Dinas Kesehatan',NULL,NULL,NULL,NULL,NULL,NULL),
	('dinsos',66,'dinsos','Dinas Sosial',NULL,NULL,NULL,NULL,NULL,NULL),
	('disdik',58,'disdik','Dinas Pendidikan',NULL,NULL,NULL,NULL,NULL,NULL),
	('disdukcapil',49,'disdukcapil','Dinas Kependudukan dan Pencatatan Sipil',NULL,NULL,NULL,NULL,NULL,NULL),
	('dishub',60,'dishub','Dinas Perhubungan',NULL,NULL,NULL,NULL,NULL,NULL),
	('diskominfo',52,'diskominfo','Dinas Komunikasi dan Informatika',NULL,NULL,NULL,NULL,NULL,NULL),
	('diskopukm',53,'diskopukm','Dinas Koperasi, Usaha Kecil dan Menengah',NULL,NULL,NULL,NULL,NULL,NULL),
	('disnaker',51,'disnaker','Dinas Ketenagakerjaan',NULL,NULL,NULL,NULL,NULL,NULL),
	('disperindag',62,'disperindag','Dinas Perindustrian dan Perdagangan',NULL,NULL,NULL,NULL,NULL,NULL),
	('dispora',48,'dispora','Dinas Kepemudaan, Olahraga dan Pariwisata',NULL,NULL,NULL,NULL,NULL,NULL),
	('dpmd',56,'dpmd','Dinas Pemberdayaan Masyarakat dan Desa',NULL,NULL,NULL,NULL,NULL,NULL),
	('dpmptsp',57,'dpmptsp','Dinas Penanaman Modal dan Pelayanan Terpadu Satu P',NULL,NULL,NULL,NULL,NULL,NULL),
	('dppkb',59,'dppkb','Dinas Pengendalian Penduduk, Keluarga Berencana, P',NULL,NULL,NULL,NULL,NULL,NULL),
	('dprd',82,'dprd','Sekretariat DPRD',NULL,NULL,NULL,NULL,NULL,NULL),
	('ekosda',89,'ekosda','Bagian Perekonomian dan SDA',NULL,NULL,NULL,NULL,NULL,NULL),
	('hukum',93,'hukum','Bagian Hukum',NULL,NULL,NULL,NULL,NULL,NULL),
	('inspektorat',68,'inspektorat','Inspektorat',NULL,NULL,NULL,NULL,NULL,NULL),
	('kesbangpol',43,'kesbangpol','Badan Kesatuan Bangsa dan Politik',NULL,NULL,NULL,NULL,NULL,NULL),
	('kesra',85,'kesra','Bagian Kesejahteraan Rakyat',NULL,NULL,NULL,NULL,NULL,NULL),
	('ketapang',67,'ketapang','Dinas Tanaman Pangan, Hortikultura dan Ketahanan P',NULL,NULL,NULL,NULL,NULL,NULL),
	('lauttador',72,'lauttador','Kecamatan Laut Tador',NULL,NULL,NULL,NULL,NULL,NULL),
	('limapuluh',73,'limapuluh','Kecamatan Lima Puluh',NULL,NULL,NULL,NULL,NULL,NULL),
	('limapuluhpesisir',74,'limapuluhpesisir','Kecamatan Lima Puluh Pesisir',NULL,NULL,NULL,NULL,NULL,NULL),
	('medangderas',75,'medangderas','Kecamatan Medang Deras',NULL,NULL,NULL,NULL,NULL,NULL),
	('nibunghangus',76,'nibunghangus','Kecamatan Nibung Hangus',NULL,NULL,NULL,NULL,NULL,NULL),
	('organisasi',84,'organisasi','Bagian Organisasi',NULL,NULL,NULL,NULL,NULL,NULL),
	('pbj',90,'pbj','Bagian Pengadaan Barang dan Jasa',NULL,NULL,NULL,NULL,NULL,NULL),
	('perikanan',61,'perikanan','Dinas Perikanan',NULL,NULL,NULL,NULL,NULL,NULL),
	('perkeu',86,'perkeu','Bagian Perencanaan dan Keuangan',NULL,NULL,NULL,NULL,NULL,NULL),
	('perkim',64,'perkim','Dinas Perumahan dan Kawasan Permukiman',NULL,NULL,NULL,NULL,NULL,NULL),
	('perpustakaan',63,'perpustakaan','Dinas Perpustakaan',NULL,NULL,NULL,NULL,NULL,NULL),
	('peternakan',65,'peternakan','Dinas Peternakan dan Perkebunan',NULL,NULL,NULL,NULL,NULL,NULL),
	('protokol',92,'protokol','Bagian Protokol dan Komunikasi Pimpinan',NULL,NULL,NULL,NULL,NULL,NULL),
	('pupr',55,'pupr','Dinas Pekerjaan Umum dan Penataan Ruang',NULL,NULL,NULL,NULL,NULL,NULL),
	('puskesindrapura',97,'puskesindrapura','Puskesmas Indrapura',NULL,NULL,NULL,NULL,NULL,NULL),
	('puskesiseibalai',101,'puskesiseibalai','Puskesmas Sei Balai',NULL,NULL,NULL,NULL,NULL,NULL),
	('puskeslabuhanruku',106,'puskeslabuhanruku','Puskesmas Labuhan Ruku',NULL,NULL,NULL,NULL,NULL,NULL),
	('puskeslalang',95,'puskeslalang','Puskesmas Lalang',NULL,NULL,NULL,NULL,NULL,NULL),
	('puskeslauttador',105,'puskeslauttador','Puskesmas Laut Tador',NULL,NULL,NULL,NULL,NULL,NULL),
	('puskeslimapuluh',107,'puskeslimapuluh','Puskesmas Lima Puluh',NULL,NULL,NULL,NULL,NULL,NULL),
	('puskespagurawan',94,'puskespagurawan','Puskesmas Pagurawan',NULL,NULL,NULL,NULL,NULL,NULL),
	('puskespematangpj',98,'puskespematangpj','Puskesmas Pematang Panjang',NULL,NULL,NULL,NULL,NULL,NULL),
	('puskespetatai',99,'puskespetatai','Puskesmas Petatal',NULL,NULL,NULL,NULL,NULL,NULL),
	('puskesseibejangkar',100,'puskesseibejangkar','Puskesmas Sei Bejangkar',NULL,NULL,NULL,NULL,NULL,NULL),
	('puskesseisuka',96,'puskesseisuka','Puskesmas Sei Suka',NULL,NULL,NULL,NULL,NULL,NULL),
	('puskessianam',104,'puskessianam','Puskesmas Kedai Sianam',NULL,NULL,NULL,NULL,NULL,NULL),
	('puskessimpangdolok',108,'puskessimpangdolok','Puskesmas Simpang Dolok',NULL,NULL,NULL,NULL,NULL,NULL),
	('puskestanjungtiram',102,'puskestanjungtiram','Puskesmas Tanjung Tiram',NULL,NULL,NULL,NULL,NULL,NULL),
	('puskesujungkubu',103,'puskesujungkubu','Puskesmas Ujung Kubu',NULL,NULL,NULL,NULL,NULL,NULL),
	('satpolpp',81,'satpolpp','Satuan Polisi Pamong Praja',NULL,NULL,NULL,NULL,NULL,NULL),
	('seibalai',77,'seibalai','Kecamatan Sei Balai',NULL,NULL,NULL,NULL,NULL,NULL),
	('seisuka',78,'seisuka','Kecamatan Sei Suka',NULL,NULL,NULL,NULL,NULL,NULL),
	('setdakab',83,'setdakab','Sekretariat Daerah',NULL,NULL,NULL,NULL,NULL,NULL),
	('skm.dukcapil',49,'skm.dukcapil','Dinas Kependudukan dan Catatan Sipil',NULL,'2021-05-18 00:00:00',NULL,NULL,NULL,NULL),
	('skm.ortala',84,'skm.ortala','MR. ORTALA',NULL,'2021-05-16 00:00:00',NULL,NULL,NULL,NULL),
	('talawi',79,'talawi','Kecamatan Talawi',NULL,NULL,NULL,NULL,NULL,NULL),
	('tanjungtiram',80,'tanjungtiram','Kecamatan Tanjung Tiram',NULL,NULL,NULL,NULL,NULL,NULL),
	('tapem',87,'tapem','Bagian Tata Pemerintahan',NULL,NULL,NULL,NULL,NULL,NULL),
	('umum',88,'umum','Bagian Umum',NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `_userinformation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_users`;

CREATE TABLE `_users` (
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_users` WRITE;
/*!40000 ALTER TABLE `_users` DISABLE KEYS */;

INSERT INTO `_users` (`UserName`, `Password`, `RoleID`, `IsSuspend`, `LastLogin`, `LastLoginIP`)
VALUES
	('admin','3798e989b41b858040b8b69aa6f2ce90',1,0,'2021-05-18 18:42:57','::1'),
	('adpem','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('airputih','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('bappeda','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('bkd','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('bpbd','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('bpkad','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('bprd','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('datuklimapuluh','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('datuktanahdatar','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('dinaskebersihan','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('dinkes','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('dinsos','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('disdik','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('disdukcapil','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('dishub','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('diskominfo','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('diskopukm','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('disnaker','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('disperindag','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('dispora','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('dpmd','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('dpmptsp','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('dppkb','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('dprd','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('ekosda','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('hukum','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('inspektorat','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('kesbangpol','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('kesra','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('ketapang','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('lauttador','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('limapuluh','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('limapuluhpesisir','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('medangderas','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('nibunghangus','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('organisasi','e10adc3949ba59abbe56e057f20f883e',2,0,'2021-05-18 19:54:19','127.0.0.1'),
	('pbj','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('perikanan','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('perkeu','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('perkim','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('perpustakaan','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('peternakan','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('protokol','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('pupr','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('puskesindrapura','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('puskesiseibalai','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('puskeslabuhanruku','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('puskeslalang','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('puskeslauttador','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('puskeslimapuluh','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('puskespagurawan','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('puskespematangpj','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('puskespetatai','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('puskesseibejangkar','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('puskesseisuka','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('puskessianam','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('puskessimpangdolok','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('puskestanjungtiram','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('puskesujungkubu','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('satpolpp','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('seibalai','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('seisuka','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('setdakab','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('skm.dukcapil','e10adc3949ba59abbe56e057f20f883e',2,1,'2021-05-18 16:48:48','127.0.0.1'),
	('skm.ortala','e10adc3949ba59abbe56e057f20f883e',2,1,'2021-05-18 15:36:07','127.0.0.1'),
	('talawi','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('tanjungtiram','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('tapem','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('umum','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL);

/*!40000 ALTER TABLE `_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table skm_mkuisioner
# ------------------------------------------------------------

DROP TABLE IF EXISTS `skm_mkuisioner`;

CREATE TABLE `skm_mkuisioner` (
  `IdKuisioner` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `NmKuisioner` text NOT NULL,
  `NmKuisionerJudul` text NOT NULL,
  `IsAktif` tinyint(1) NOT NULL DEFAULT '1',
  `Seq` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`IdKuisioner`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `skm_mkuisioner` WRITE;
/*!40000 ALTER TABLE `skm_mkuisioner` DISABLE KEYS */;

INSERT INTO `skm_mkuisioner` (`IdKuisioner`, `NmKuisioner`, `NmKuisionerJudul`, `IsAktif`, `Seq`)
VALUES
	(1,'Bagaimana pendapat saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya?','Persyaratan Pelayanan',1,1),
	(2,'Bagaimana pemahaman saudara tentang kemudahan prosedur pelayanan di unit ini?','Prosedur Pelayanan',1,2),
	(3,'Bagaimana pendapat saudara tentang kecepatan waktu dalam memberikan pelayanan?','Waktu Pelayanan',1,3),
	(4,'Bagaimana pendapat saudara tentang kewajaran biaya / tarif dalam pelayanan?','Biaya / Tarif Pelayanan',1,4),
	(5,'Bagaimana pendapat saudara tentang produk pelayanan antara yang tercantum dalam standart pelayanan dengan hasil yang diberikan?','Produk Jenis Pelayanan',1,5),
	(6,'Bagaimana pendapat saudara tentang kompetensi / kemampuan petugas dalam pelayanan?','Kompetensi Pelaksana',1,6),
	(7,'Bagaimana pendapat saudara tentang perilaku petugas dalam pelayanan terkait kesopanan dan keramahan?','Perilaku Pelaksana Pelayanan',1,7),
	(8,'Bagaimana pendapat saudara tentang kualitas sarana dan prasarana?','Sarana dan Prasarana',1,8),
	(9,'Bagaimana pendapat saudara tentang penanganan pengaduan pengguna layanan?','Penanganan Pengaduan dan Saran',1,9);

/*!40000 ALTER TABLE `skm_mkuisioner` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table skm_mkuisioneropt
# ------------------------------------------------------------

DROP TABLE IF EXISTS `skm_mkuisioneropt`;

CREATE TABLE `skm_mkuisioneropt` (
  `IdOpt` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdKuisioner` bigint(10) unsigned NOT NULL,
  `NmOpt` text NOT NULL,
  `Skor` double NOT NULL,
  PRIMARY KEY (`IdOpt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `skm_mkuisioneropt` WRITE;
/*!40000 ALTER TABLE `skm_mkuisioneropt` DISABLE KEYS */;

INSERT INTO `skm_mkuisioneropt` (`IdOpt`, `IdKuisioner`, `NmOpt`, `Skor`)
VALUES
	(1,1,'Tidak Sesuai',1),
	(2,1,'Kurang Sesuai',2),
	(3,1,'Sesuai',3),
	(4,1,'Sangat Sesuai',4),
	(5,2,'Tidak Mudah',1),
	(6,2,'Kurang Mudah',2),
	(7,2,'Mudah',3),
	(8,2,'Sangat Mudah',4),
	(11,3,'Tidak Cepat',1),
	(12,3,'Kurang Cepat',2),
	(13,3,'Cepat',3),
	(14,3,'Sangat Cepat',4),
	(15,4,'Sangat Mahal',1),
	(16,4,'Cukup Mahal',2),
	(17,4,'Murah',3),
	(18,4,'Gratis',4),
	(19,5,'TIdak Sesuai',1),
	(20,5,'Kurang Sesuai',2),
	(21,5,'Sesuai',3),
	(22,5,'Sangat Sesuai',4),
	(23,6,'Tidak Kompeten',1),
	(24,6,'Kurang Kompeten',2),
	(25,6,'Kompeten',3),
	(26,6,'Sangat Kompeten',4),
	(27,7,'Tidak Sopan dan Ramah',1),
	(28,7,'Kurang Sopan dan Ramah',2),
	(29,7,'Sopan dan Ramah',3),
	(30,7,'Sangat Sopan dan Ramah',4),
	(31,8,'Buruk',1),
	(32,8,'Cukup',2),
	(33,8,'Baik',3),
	(34,8,'Sangat Baik',4),
	(35,9,'Tidak Ada',1),
	(36,9,'Ada Namun Tidak Berfungsi',2),
	(37,9,'Berfungsi Kurang Maksimal',3),
	(38,9,'Dikelola dengan Baik',4);

/*!40000 ALTER TABLE `skm_mkuisioneropt` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table skm_mlayanan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `skm_mlayanan`;

CREATE TABLE `skm_mlayanan` (
  `IdLayanan` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdOPD` bigint(10) unsigned NOT NULL,
  `NmLayanan` varchar(200) NOT NULL DEFAULT '',
  `IsAktif` tinyint(1) NOT NULL DEFAULT '1',
  `CreatedBy` varchar(50) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`IdLayanan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `skm_mlayanan` WRITE;
/*!40000 ALTER TABLE `skm_mlayanan` DISABLE KEYS */;

INSERT INTO `skm_mlayanan` (`IdLayanan`, `IdOPD`, `NmLayanan`, `IsAktif`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(3,87,'Penyusunan Perjanjian Kerjasama / MOU',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(4,87,'Penyampaian Informasi Kewilayahan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(5,87,'Permohonan Audiensi Dengan Kepala Daerah',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(6,85,'Bantuan Hibah Rumah Ibadah Dan Organisasi Keagamaan Serta Bantuan Sosial',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(7,93,'Fasilitasi Penyusunan Produk Hukum Daerah (Perda, Perbup,Keputusan Bupati)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(8,93,'Pelayanan Informasi Hukum',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(9,93,'Pelaksanaan Penyelesaian Kasus-Kasus Hukum',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(10,89,'Koordinasi/Konsultasi Data/Informasi Badan Usaha Milik Daerah (Bumd) Di Kabupaten Batu Bara',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(11,89,'Koordinasi/ Konsultasi Data/ Informasi Perekonomian Di Kabupaten Batu Bara',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(12,89,'Koordinasi Dan Konsultasi Terkait Dengan Konservasi Sda Di Kabupaten Batu Bara',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(13,91,'Konsultasi Tatap Muka Penyusunan Standar Harga Satuan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(14,91,'Konsultasi Tatap Muka Penyusunan Informasi Data Pembangunan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(15,91,'Konsultasi Tatap Muka Monitoring Pengendalian Pembangunan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(16,90,'Pelayanan Helpdesk (Layanan Dan Dukungan)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(17,90,'Pelayanan Registrasi Dan Verifikasi Calon Penyedia',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(18,90,'Pelayanan Pembuatan Akun Admin Agency',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(19,90,'Pelayanan Pelatihan E-Procurement',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(20,88,'Permohonan Pinjam Pakai Mobil',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(21,88,'Permohonan Pinjam Pakai Aula',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(22,88,'Penomoran Dokumen',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(23,84,'Penyusunan Standar Operasional Prosedur Administrasi Pemerintah (OP AP)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(24,84,'Penyusunan Standar Pelayanan Perangkat Daerah',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(25,84,'Penyusunan Peta Proses Bisnis Instansi Pemerintah',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(26,84,'Penyusunan Survei Kepuasan Masyarakat',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(27,84,'Konsultasi Tata Naskah Dinas',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(28,84,'Penggunaan Pakaian Dinas',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(29,84,'Penyusunan ANJAB dan ABK',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(30,84,'Penyusunan Evaluasi Jabatan (Evjab)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(31,84,'Penyusunan Tugas Pokok Dan Fungsi (Tupoksi)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(32,84,'Penyusunan Penilaian Perangkat Daerah',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(33,84,'Penataan Perangkat Daerah',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(34,84,'Penyusunan Road Map Reformasi Birokrasi',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(35,84,'Penyusunan Laporan Kinerja (LK)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(36,92,'Permintaan Dokumentasi Pimpinan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(37,92,'Penyusunan Konsep Pidato Dan Jadwal Kegiatan Pimpinan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(38,86,'Penyusunan Rencana Kerja Dan Anggaran (RKA)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(39,86,'Pencairan GU',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(40,86,'Pencairan LS',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(41,82,'Informasi Publik',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(42,82,'Pengaduan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(43,82,'Penerimaan Kunjungan Kerja',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(44,82,'Pengganti Antar Waktu',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(45,82,'Pemrosesan Ranperda Inisiatif',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(46,68,'Pemeriksaan Dengan Tujuan Tertentu (PEMSUS)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(47,68,'Pemeriksaan Kinerja',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(48,68,'Reviu LKPD',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(49,68,'Reviu LAKIP Pemda',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(50,68,'Reviu RKA SKPD dan PPKD',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(51,68,'Evaluasi LAKIP',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(52,68,'Pemutakhiran Data Tindak Lanjut Hasil Pengawasan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(53,68,'Evaluasi Berkala Temuan Hasil Pengawasan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(54,68,'Pengelolaan LHKPN',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(55,58,'Layanan Mutasi Siswa',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(56,58,'Surat Keterangan Pengganti STTB / IJAZAH / DANEM / SKHU / SKYBS',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(57,58,'Magang / PKL /KKN / Penelitian',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(58,58,'Nomor Pokok Sekolah Nasional (NPSN)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(59,58,'Rekomendasi Mutasi',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(60,58,'Tunjangan Pendidik',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(61,58,'Rekomendasi Teknis Izin Pendirian Satuan Pendidikan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(62,58,'Pembuatan Kartu Pegawai',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(63,58,'Pengesahan Kurikulum Tingkat Satuan Pendidikan (KTSP)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(64,58,'Legalisir STTB / IJAJAH / DANEM / SKHU / SKYBS',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(65,58,'Surat Keterangan Penelitian',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(66,58,'Rekomendasi sekolah',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(67,58,'Rekomendasi Pendirian Satuan Pendidikan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(68,58,'Rekomendasi Izin Operasional Satuan Pendidikan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(69,58,'Lembaga Pendidikan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(70,58,'Rekomendasi Mutasi Siswa',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(71,58,'Layanan BOS',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(72,58,'Layanan PPDB',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(73,58,'Pelayanan Dapodik',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(74,58,'Sertifikasi Pendidik',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(75,50,'Rekomendasi Izin Praktik Dan Kerja Okupasi Terapi',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(76,50,'Rekomendasi Izin Praktik Dan Kerja Ortotis Prostetis',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(77,50,'Rekomendasi Izin Praktik Dan Kerja Terafis Wicara',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(78,50,'Rekomendasi Izin Praktik Dan Kerja Radiografer',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(79,50,'Rekomendasi Izin Praktik Dan Kerja Fisioterapis',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(80,50,'Rekomendasi Izin Praktik Elektromedis',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(81,50,'Rekomendasi Izin Perekam Medis',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(82,50,'Rekomendasi Izin Kerja Tenaga Sanitarian',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(83,50,'Rekomendasi Izin Psikolog Klinis',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(84,50,'Rekomendasi Izin Praktik/ Kerja Tenaga Gizi',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(85,50,'Pelayanan Rekomendasi Ijin Kerja Refraksionis Optisien / Optometris',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(86,50,'Pelayanan Rekomendasi Ijin Praktek Perawat Penata Anastesi',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(87,50,'Pelayanan Izin Praktek Dokter / Dokter Gigi',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(88,50,'Pelayanan Rekomendasi Ijin Praktek Bidan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(89,50,'Pelayanan Rekomendasi Ijin Praktek Perawat',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(90,50,'Pelayanan Rekomendasi Sertifikat Laik Higine Sanitasi Hotel',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(91,50,'Pelayanan Rekomendasi Laik Higiene Sanitasi Rumah Makan Dan Restoran',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(92,50,'Pelayanan Rekomendasi Sertifikat Laik Higiene Sanitasi Jasaboga',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(93,50,'Pelayanan Rekomendasi Laik Higiene Sanitasi Depot Air Minum',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(94,50,'Pelayanan Rekomendasi Stpt Pijat Tradisional',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(95,50,'Pelayanan Rekomendasi Stpt Bekam',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(96,50,'Pelayanan Rekomendasi Stpt Ramuan Herbal',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(97,50,'Pelayanan Rekomendasi Izin Spa Tradisonal',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(98,50,'Pelayanan Rekomendasi Stpt Terapis',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(99,50,'Pelayanan Rekomendasi Stpt Akupuntur Terafis',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(100,50,'Pelayanan Rekomendasi Izin Tenaga Kesehatan Tradisional',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(101,50,'Pelayanan Rekomendasi Izin Griya Sehat',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(102,50,'Pelayanan Rekomendasi Izin Sarana Griya Sehat',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(103,50,'Pelayanan Rekomendasi Izin Panti Sehat',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(104,50,'Pelayanan Surat Izin Praktik Tenaga Teknis Kefarmasian',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(105,50,'Pelayanan Rekomendasi Surat Izin Praktik Apoteker (Sipa)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(106,50,'Pelayanan Rekomendasi Surat Izin Apotik',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(107,50,'Pelayanan Rekomendasi Surat Izin Toko Obat (SITO)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(108,50,'Pelayanan Rekomendasi Sertifikat Produksi Pangan Industri Rumah Tangga (SPP-IRT)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(109,50,'Pelayanan Rekomendasi Surat Izin Usaha Mikro Obat Tradisional (UMOT)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(110,50,'Pelayanan Rekomendasi Izin Perusahaan Rumah Tangga Alat Kesehatan (PRT-ALKES) Dan Perbekalan Kesehatan Rumah Tangga (PKRT)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(111,55,'Surat Rekomendasi Teknis Izin Mendirikan Bangunan (IMB)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(112,55,'Penerbitan Rekomendasi Teknis Kesesuaian Tata Ruang\n',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(113,55,'Rekomendasi Izin Usaha Jasa Kontruksi (IUJK)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(114,55,'Pemakaian Kekayaan Daerah Sewa Alat Berat\n',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(115,55,'Surat Perintah Membayar Langsung (SPM-LS) Barang / Jasa',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(116,64,'Pelayanan Pengusulan Bantuan Rumah Swadaya',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(117,64,'Rekomendasi Advis Izin Mendirikan Bangunan Rumah Tempat Tinggal',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(118,64,'Penyerahan Fasilitas Prasarana Sarana Dan Utilitas Perumahan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(119,64,'Pengajuan SPM-LS Pembayaran ke Penyedia Barang / Jasa',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(120,81,'Pencegahan, Penanggulangan, Penyelamatan Kebakaran Dan Penyelamatan Non Kebakaran',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(121,81,'Penegakan Peraturan Perundang-Undangan Daerah',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(122,66,'Pemberian Bantuan Terhadap Lanjut Usia',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(123,66,'Pemberian Bantuan Anak Putus Sekolah',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(124,66,'Jenis Pelayanan\nPemberian Bantuan Terhadap Penyandang Cacat/ Disabilitas\n',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(125,66,'Pelayanan Pemberian Bantuan Korban Bencana Dan Sosial\n',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(126,66,'Pemberian Bantuan Rekomendasi/Rujukan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(127,66,'Rekomendasi Verifikasi Dan Validasi Pendirian Lks',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(128,51,'Pencatatan Perjanjian Kerja Waktu Tertentu (PKWT)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(129,51,'Pengesahan Peraturan Perusahaan (PP)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(130,51,'Pendaftaran Perjanjian Kerja Bersama (PKB)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(131,51,'Pencatatan Serikat Pekerja/Serikat Buruh',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(132,51,'Penyelesaian Kasus Perselisihan Hubungan Industrial',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(133,51,'Bukti Pelaporan Jenis Pekerjaan Penunjang',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(134,51,'Pendaftaran Perjanjian Pemborongan Pekerjaan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(135,51,'Pendaftaran Perjanjian Penyediaan Jasa Pekerja / Buruh',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(136,51,'Pembuatan Kartu Pencari Kerja / Kartu Ak-1 / Kartu Kuning',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(137,51,'Penerbitan Rekomendasi Passport CTKI',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(138,51,'Pendaftaran Calon Peserta Pelatihan Keterampilan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(139,51,'Pengaduan Masyarakat Terhadap Pelayanan Ketenagakerjaan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(140,59,'Pelayanan Konseling KB',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(141,59,'Pelayanan Pendampingan Kasus',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(142,67,'Penumbuhan Kelompok Tani',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(143,67,'Pelayanan Pendampingan Pembuatan RDKK',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(144,67,'Pelayanan Penilaian Kelas Kelompok Tani',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(145,67,'Pelayanan Pengadaan Bibit Tanaman Pangan Dan Hortikultura',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(146,67,'Pelayanan Permohonan Bantuan Pestisida',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(147,67,'Pelayanan Penyediaan Infrastruktur Pendukung Kemandirian Pangan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(148,67,'Pelayanan Penyediaan P2L',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(149,67,'Pelayanan Cadangan Pangan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(150,67,'Pelayanan Konstratani',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(151,67,'Pelayanan Penyediaan Instalasi Hidroponik',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(152,67,'Pelayanan Permohonan Penerbitan Kartu Tani',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(153,67,'Pelayanan Usulan Penangkar Benih',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(154,67,'Pelayanan Rekomendasi Pemupukan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(155,67,'Pelayanan Pengklaiman AUTP',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(156,67,'Pelayanan Pelatihan BPP',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(157,54,'Pemungutan PAD Persampahan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(158,54,'Pengangkutan Sampah',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(159,54,'Pemantauan Air Sungai',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(160,54,'Rekomendasi Izin Lingkungan (UKL-UPL)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(161,54,'Pembuatan Surat Pernyataan Pengelolaan Lingkungan (SPPL)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(162,54,'Rekomendasi Izin Pembuangan Air Limbah ke Air atau Sumber Air (Media Lingkungan yang Bukan Air)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(163,54,'Rekomendasi Izin Penyimpanan Sementara Dan Pengumpul Limbah Bahan Berbahaya Beracun (TPS LB3)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(164,54,'Pengaduan Dan Sengketa Lingkungan Hidup',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(165,49,'Surat Keterangan Pindah',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(166,49,'Kartu Keluarga',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(167,49,'Akta Kematian',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(168,49,'Kartu Tanda Penduduk (KTP)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(169,49,'Surat Keterangan Tidak Mampu (SKTM)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(170,49,'Akta Kelahiran',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(171,49,'Akta Perceraian',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(172,49,'Akta Pengakuan, Pengesahan, dan Pengangkatan Anak',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(173,49,'Surat Keterangan Waris',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(174,49,'Akta Perkawinan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(175,49,'Surat Keterangan Belum Menikah',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(176,49,'Surat Keterangan Janda/Duda',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(177,49,'Data Kependudukan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(178,49,'Kartu Identitas Anak (KIA)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(179,49,'Administrasi Kependudukan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(180,49,'Surat Rekomendasi Dispensasi Nikah (Kurang dari 10 Hari setelah daftar)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(181,49,'Surat Keterangan Domisili',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(182,49,'Surat Keterangan Penghasilan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(183,49,'Surat keterangan untuk pengajuan kredit ',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(184,49,'Biodata Penduduk',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(185,56,'Alur Pelayanan Penyaluran APBDESA (ADD dan DD)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(186,56,'Pengaduan Tentang Pemberhentian Dan Pengangkatan Perangkat Desa',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(187,56,'Pengaduan Tentang Sengketa Pilkades',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(188,62,'Izin Usaha Industri (IUI)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(189,62,'Pelayanan Tera dan Tera Ulang',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(190,62,'Penerbitan Kartu Pedagang Pasar',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(191,62,'Surat Rekomendasi Izin Usaha',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(192,60,'Izin Pengelola Parkir Khusus',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(193,60,'Izin Pengelola Parkir Di Tepi Jalan Umum',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(194,60,'Rekomendasi Izin Trayek',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(195,60,'Rekomendasi Izin Penyelenggaraan Angkutan Orang',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(196,60,'Rekomendasi Izin Usaha Jasa Angkutan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(197,60,'Izin Insidentil',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(198,60,'Rekomendasi Penyelenggaraan Angkutan Penyeberangan ',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(199,60,'Rekomendasi Pengoperasian Pelabuhan Pengumpan Lokal',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(200,60,'Rekomendasi Izin Pengerukan Pelabuhan Pengumpan Lokal',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(201,60,'Surat Rekomendasi Izin Trayek Usaha Angkutan Sungai Danau Dan Penyeberangan (ASDP)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(202,60,'Rekomendasi Analisis Dampak Lalu Lintas (Andalalin)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(203,60,'Uji Berkala Kendaraan Bermotor Pertama',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(204,60,'Uji Berkala Kendaraan Bermotor Regular',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(205,60,'Numpang Uji Masuk',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(206,60,'Numpang Uji Keluar',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(207,60,'Mutasi Masuk',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(208,60,'Mutasi Keluar',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(209,60,'Pelayanan Servis Mesin, Bodi Dan Kelistrikan Kapal',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(210,60,'Pelayanan Penitipan Kapal Dan Atau Tambat Labuh Kapal',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(211,60,'Pengoperasian Terminal Penumpang Angkutan Jalan Tipe C',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(212,52,'Pelayanan Rapat Daring (Meeting Online)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(213,52,'Pelayanan Pendaftaran Layanan Server (Hosting) Dan Sub Domain Batubarakab.go.id',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(214,52,'Pelayanan Pendaftaran Alamat Surat Elektronik (E-Mail) Batu Bara',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(215,52,'Pelayanan Pendaftaran dan Perubahan Data E- Absensi',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(216,52,'Pelayanan Pendaftaran dan Perubahan Administrator OPD pada Aplikasi Surat Elektronik Kabupaten Batu Bara (Sekabar)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(217,52,'Pelayanan Sistem Telekomunikasi Internal (VOIP)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(218,52,'Pelayanan Permohonan Informasi dan Dokumentasi Publik',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(219,52,'Pelayanan Lembaga Penyiaran Publik Lokal (LPPL) Radio Odan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(220,52,'Pelayanan Penyelenggaraan Pelayanan Pengacak Sinyal (Jamming)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(221,52,'Pelayanan Penanganan Insiden Keamanan Sistem Elektronik',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(222,53,'Pengurusan Surat Rekomendasi Izin Usaha Simpan Pinjam ',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(223,53,'Pengurusan Izin Pembukaan Kantor Cabang',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(224,53,'Pengurusan Surat Rekomendasi Izin Pembukaan Kantor \nCabang Pembantu\n',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(225,53,'Pengurusan Surat Rekomendasi Izin Pembukaan Kantor Kas',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(226,53,'Pengurusan Surat Rekomendasi Sertifikat Hak Atas Kekayaan Intelektual (HAKI)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(227,53,'Pengurusan Surat Rekomendasi Sertifikat Halal',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(228,53,'Pengurusan Surat Rekomendasi Sertifikat Pirt',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(229,57,'Izin Usaha Pertambangan (IUP) Operasi Produksi',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(230,57,'Izin Usaha Pertambangan (IUP) Eksplorasi',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(231,57,'Izin Angkutan Tidak Dalam Trayek',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(232,57,'Surat Izin Penangkapan Ikan (SIPI)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(233,57,'Izin Usaha Angkutan Orang Dalam Trayek ',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(234,57,'Izin Usaha Penyediaan Tenaga Listrik (IUPTL)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(235,57,'Izin Apotek, Toko Obat, Toko Alat Kesehatan dan Optika',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(236,57,'Izin Pertambangan Khusus untuk Pengolahan Mineral Bukan Logam dan Batuan (Badan Usaha)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(237,57,'Izin Usaha Industri (IUI)/Tanda Daftar Industri (TDI)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(238,57,'Izin Lembaga Penempatan Tenaga Kerja Swasta (ILPTKS)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(239,57,'Izin Koperasi Simpan Pinjam (KSP)/Unit Simpan Pinjam Koperasi (USP)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(240,57,'Surat lzin Usaha Perikanan (SIUP)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(241,57,'Izin Pembangunan Prasarana Perkeretaapian Umum/Khusus',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(242,57,'Izin Usaha Perkebunan Budidaya (IUPB)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(243,57,'Izin Kantor Cabang, Cabang Pembantu dan Kantor Kas Koperasi Simpan Pinjam',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(244,57,'Izin Usaha Obat Hewan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(245,57,'Wilayah Izin Usaha Pertambangan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(246,57,'Izin Usaha Perkebunan (IUP)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(247,57,'Izin Pertambangan Khusus untuk Pengangkutan dan Penjualan Mineral Bukan Logam dan Batuan (Badan Usaha)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(248,57,'Izin Pengelolaan Pasar Rakyat, Pusat Perbelanjaan dan Izin Usaha Toko Modern',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(249,57,'Izin Lembaga Pendidikan Formal dan non Formal ',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(250,57,'Izin Mendirikan Bangunan (IMB)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(251,57,'Izin Pemanfaatan Jasa Lingkungan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(252,57,'Izin Lingkungan Hidup (Amdal/UKL/UPL/SPPL)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(253,57,'Izin Pengelolaan Terminal Untuk Kepentingan Sendiri (TUKS) didalam ',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(254,57,'DLKR/DLKP Pelabuhan Pengumpan Regional',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(255,57,'Izin Praktik Dokter Umum/Dokter Gigi/Dokter Spesialis/Gigi Spesialis',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(256,57,'Izin Lokasi',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(257,57,'Surat lzin Kerja perawat (SIKP) & Surat izin Praktik Perawat (SIPP)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(258,57,'Izin Penelitian',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(259,57,'Izin Mendirikan Rumah Sakit',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(260,57,'Izin Belajar',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(261,57,'Izin Pembuangan Limbah Cair (SIPLC)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(262,57,'Izin Operasional Pengelolaan Limbah Bahan Berbahaya dan Beracun (LB3) untuk Kegiatan Penyimpanan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(263,57,'Izin Operasional Rumah Sakit',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(264,57,'Izin Operasional Yayasan/Panti Asuhan/Lembaga Kesejahteraan Sosial (LKS)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(265,57,'Surat Izin Praktik Bidan (SIPB)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(266,57,'Izin Pengembangan Pelabuhan Pengumpan Regional',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(267,57,'Izin Keramaian',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(268,57,'Surat Izin Praktik/Kerja Fisioterapis (SIPF/SIKF) ',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(269,57,'Surat Izin Praktik/Kerja Radiografer (SIPR)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(270,48,'Pelayanan Tanda Daftar Usaha Jasa Makanan & Minuman',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(271,48,'Pelayanan Tanda Daftar Usaha Jasa Perjalanan Wisata',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(272,48,'Pelayanan Tanda Daftar Usaha Jasa Wisata Tirta & Spa',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(273,48,'Pelayanan Tanda Daftar Usaha Jasa Informasi Dan Konsultan Pariwisata',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(274,48,'Pelayanan Tanda Daftar Usaha Jasa Penyedia Akomodasi',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(275,63,'Informasi, Aduan Masyarakat dan Bimbingan Pemustaka',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(276,63,'Kunjungan Perpustakaan / Wisata Pustaka',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(277,63,'Keanggotaan Perpustakaan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(278,63,'Loker Penitipan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(279,63,'Sirkulasi (Peminjaman Dan Pengembalian Bahan Pustaka)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(280,63,'Baca di tempat Koleksi Bahan Pustaka',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(281,63,'Digital E-Book (ePusda Batu Bara)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(282,63,'Digital (Internet)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(283,63,'Perpustakaan Keliling',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(284,63,'Paket Pinjam Sementara Buk',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(285,61,'Penerbitan Surat Rekomendasi Izin Usaha Perikanan (Iup) Budidaya',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(286,61,'Penerbitan Tanda Daftar Pembudidaya Ikan Kecil',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(287,61,'Penerbitan Tanda Daftar Kapal Perikanan (TDKP)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(288,61,'Penerbitan Rekomendasi Pembelian BBM Jenis Tertentu',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(289,61,'Penerbitan Rekomendasi Pembangunan SPDN / SPBUN',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(290,61,'Penerbitan Surat Rekomendasi Tanda Daftar Usaha Pengolahan Hasil Perikanan (TDUPHP)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(291,61,'Bantuan Premi Asuransi Nelayan (BPAN)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(292,61,'Kartu Asuransi Nelayan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(293,61,'Pembuatan Kartu Pelaku Utama Sektor Kelautan Dan Perikanan (Kusuka) Bagi Pembudidaya Ikan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(294,61,'Pembuatan Kartu Pelaku Utama Sektor Kelautan Dan Perikanan (Kusuka) Bagi Nelayan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(295,61,'Pembuatan Kartu Pelaku Utama Sektor Kelautan Dan Perikanan (Kusuka) Bagi Pengolah Dan Pemasar Hasil Perikanan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(296,65,'Standar Pelayanan Seleksi Calon Penerima Calon Lokasi (CPCL) Kelompok Tani Ternak Penerima Bantuan Pemerintah',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(297,65,'Standar Pelayanan Inseminasi Buatan (Kawin Suntik) Pada Ternak Ruminansia',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(298,65,'Standar Pelayanan Pemeriksaan Kebuntingan Pada Ternak Ruminansia Besar',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(299,65,'Standar Pelayanan Monitoring Dan Pengawasan Bantuan Ternak Dana Atau Hijauan Pakan Ternak Pemerintah',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(300,65,'Standar Pelayanan Inseminasi Buatan Pada Daerah Introduksi',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(301,65,'Standar Pelayanan Aktif Kesehatan Hewan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(302,65,'Standar Pelayanan Pasif Kesehatan Hewan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(303,65,'Standar Pelayanan Penerbitan Rekomendasi Peredaran Obat Hewan, Vaksin Dan Bahan Diagnotis Untuk Hewan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(304,65,'Standar Pelayanan Penerbitan Rekomendasi Instalasi Karantina Hewan Sementara (Ikhs)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(305,65,'Standar Pelayanan Penerbitan Rekomendasi Melaksanakan Pelayanan Jasa Medik Veteriner Dan Paramedik Veteriner',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(306,65,'Standar Pelayanan Penerbitan Surat Keterangan Kesehatan Hewan (SKKH)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(307,65,'Standar Pelayanan Penerbitan Surat Keterangan Kesehatan Pangan Asal Hewan (SKK-PAH)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(308,65,'Standar Pelayanan Pengawasan Aktif Pemotongan Hewan Dan Pangan Asal Hewan (PAH)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(309,65,'Standar Pelayanan Penerbitan Surat Keterangan Pemenuhan Tempat Praktik Dokter Hewan Mandiri Dan Surat Keterangan Pemenuhan Persyaratan Tempat Pelayanan Paramedik Veteriner',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(310,65,'Standar Pelayanan Penerbitan Surat Keterangan Pemenuhan Persyaratan Teknis Untuk Sivet (Ambulatori / Klinik Hewan / Rumah Sakit Hewan)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(311,65,'Standar Pelayanan Aktif Puskeswan.',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(312,65,'Standar Pelayanan Pasif Puskeswan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(313,65,'Standar Pelayanan Asuransi Usaha Ternak Sapi (Auts)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(314,65,'Standar Pelayanan Pasar Hewan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(315,65,'Standar Pelayanan Surat Pertimbangan Teknis Izin Persetujuan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(316,65,'Standar Pelayanan Surat Pertimbangan Teknis Izin Persetujuan Penambahan Kapasitas Industri Pengolahan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(317,65,'Standar Pelayanan Surat Pertimbangan Teknis Izin Usaha Industri Pengolahan Hasil Perkebunan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(318,65,'Standar Pelayanan Surat Pertimbangan Teknis Izin Usaha Perkebunan Yang Terintegrasi Antara Budi Daya Dengan Industri Pengolahan Hasil Perkebunan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(319,65,'Standar Pelayanan Surat Pertimbangan Teknis Izin Usaha Budi Daya Tanaman Perkebunan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(320,65,'Standar Pelayanan Surat Persetujuan Penerbitan Benih Kelapa Sawit (SP2BKS)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(321,65,'Standar Pelayanan Surat Pertibangan Teknis Izin Perubahan Jenis Tanaman',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(322,65,'Standar Pelayanan Surat Pertimbangan Teknis Izin Perubahan Luas Lahan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(323,45,'Pencairan Dana Hibah Dan Bantuan Sosial',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(324,45,'Asistensi Penyusunan Rencana Kerja Dan Anggaran (RKA-OPD)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(325,45,'Penyusunan DPA Dan DPPA',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(326,45,'Pengendalian Anggaran Belanja OPD Dengan Penerbitan SPD',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(327,45,'Verifikasi SPM dan Cetak SP2D',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(328,45,'Pelayanan Gaji PNS Dan SKPP',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(329,45,'Pinjam Pakai Gedung Kantor',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(330,45,'Penghapusan Barang Dari Daftar Inventaris Barang Milik Daerah',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(331,45,'Asistensi Penyusunan Laporan Keuangan Pemerintah Daerah Dan SKPD',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(332,47,'Penginputan Musrenbang Desa / Kelurahan Pada SIPD',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(333,47,'Pelayanan Konsultasi',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(334,47,'Pelayanan Studi Banding',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(335,44,'Rekomendasi peil / bebas banjir',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(336,44,'Pelayanan evakuasi bencana',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(337,44,'Pelayanan pengaduan bencana',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(338,43,'Rekomendasi Izin Penelitian',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(339,43,'Surat Keterangan Terdaftar',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(340,77,'Surat Keterangan Tidak Mampu (SKTM)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(341,77,'Izin Mendirikan Bangunan (IMB)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(342,77,'Izin Usaha Pariwisata',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(343,77,'Izin Hiburan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(344,76,'Surat Dispensasi Nikah',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(345,76,'Pembuatan Permohonan Menjadi Pelanggan Tarif Bersubsidi',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(346,76,'Surat Keterangan Tidak Mampu (SKTM)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(347,76,'Surat Penyerahan/Ganti Rugi',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(348,76,'Izin Usaha',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(349,76,'Surat Izin Mendirikan Bangunan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(350,76,'Surat Pindah Wni',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(351,76,'Pengelolaan Dana Bagi Hasil Pajak Daerah (DBH)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(352,76,'Surat Keterangan Tanah',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(353,76,'Pengelolaan Anggaran Dana Desa (ADD)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(354,76,'Surat Izin Hiburan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(355,76,'Pengelolaan Dana Desa (DD)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(356,80,'Surat Keterangan Tidak Mampu (SKTM)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(357,80,'Surat Keterangan Ahli Waris',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(358,80,'Surat Keterangan Kematian',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(359,80,'Rekomendasi Izin Mendirikan Bangunan (IMB)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(360,80,'Surat Keterangan Tanah ( SKT)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(361,80,'Surat Keterangan Ganti Rugi Tanah',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(362,80,'Rekomendasi Pengusulan Perangkat Desa',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(363,80,'Rekomendasi Pencairan SILTAP, Tunjangan Kades Dan Perangkat Desa',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(364,80,'Permohonan Pencairan Alokasi  Dana Desa (ADD)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(365,80,'Perekaman Kartu Tanda Penduduk (KTP)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(366,79,'Surat Keterangan Tidak Mampu (SKTM)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(367,79,'Izin Mendirikan Bangunan (IMB)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(368,79,'Izin Usaha Pariwisata',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(369,79,'Izin Hiburan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(370,71,'Izin Mendirikan Bangunan (IMB)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(371,71,'Keterangan Kepesertaan Listrik Bersubsidi',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(372,71,'Izin Hiburan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(373,70,'Mengeluarkan Rekom Alokasi Dana Desa (ADD)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(374,70,'Prosedur Rekomendasi Surat Keterangan Tidak Mampu',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(375,70,'Prosedur Penerbitan Rekomendasi Surat Ijin Hiburan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(376,70,'Izin Mendirikan Bangunan (IMB)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(377,70,'Permohonan  Pendaftaran Objek Pajak Baru Pbb',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(378,70,'Permohonan Umkm',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(379,70,'Pembuatan Surat Keterangan Tanah (SKT)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(380,74,'Surat Keterangan Tidak Mampu (SKTM)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(381,74,'Izin Mendirikan Bangunan (IMB)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(382,74,'Izin Usaha Pariwisata',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(383,74,'Izin Hiburan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(384,78,'Penerbitan Dispensasi Nikah',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(385,78,'Penerbitan Rekomendasi Izin Mendirikan Bangunan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(386,78,'Penerbitan Rekomendasi Izin Gangguan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(387,78,'Penerbitan Surat Keterangan Tidak Mampu',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(388,78,'Penerbitan Surat Pindah',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(389,78,'Perekaman E-KTP',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(390,78,'Lembar Verifikasi Dan Surat Pengantar ADD',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(391,78,'Lembar Verifikasi Dan Surat Pengantar DD',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(392,78,'Lembar Pengaduan Subsidi Listrik',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(393,72,'Surat Keterangan Tidak Mampu (SKTM)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(394,72,'Rekomendasi Izin Mendirikan Bangunan (IMB)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(395,72,'Surat Keterangan Tanah',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(396,72,'Rekomendasi Pengusulan Perangkat Desa',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(397,72,'Rekomendasi Pencairan Siltap , Tunjangan Kades',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(398,72,'Permohonan Pencairan Alokasi Dana Desa (ADD)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(399,72,'Perekaman Kartu Tanda Penduduk (KTP)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(400,75,'Surat Keterangan Tidak Mampu (SKTM)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(401,75,'Surat Keterangan Kematian',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(402,75,'Surat Keterangan Ahli Waris',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(403,75,'Surat Keterangan Dispensasi Nikah',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(404,75,'Izin Keramaian / Hiburan',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(405,75,'Surat Keterangan Bersih Diri (SKBD) Untuk Pelamaran TNI / Polri',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(406,75,'Surat Keterangan Catatan Kepolisian (SKCK)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(407,75,'Rekomendasi Izin Mendirikan Bangunan (IMB)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(408,75,'Pengaduan Keperluan Listrik Rumah Tangga',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(409,75,'Surat Keterangan Hak Kapal',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(410,75,'Surat Rekomendasi Siup/Situ',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(411,75,'Izin Usaha Kecil Menengah / Mikro',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(412,75,'Permohonan Pencairan Alokasi Dana Desa (ADD)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(413,75,'Surat Rekomendasi Pencairan Dana Bagi Hasil (DBH)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(414,75,'Surat Rekomendasi Pencairan Dana Desa (DD)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(415,75,'Rekomendasi Pengusulan Perangkat Desa',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(416,75,'Rekomendasi Pencairan Siltap, Tunjangan Kades Dan Perangkat Desa',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(417,75,'Perekaman Kartu Penduduk (KTP)',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(418,75,'Rekomendasi Penduduk Pindah',1,'admin','2021-05-18 09:00:00',NULL,NULL),
	(419,94,'Pelayanan Kesehatan Ibu dan Anak / KB',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(420,94,'Pelayanan Posyandu Balita dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(421,94,'Pelayanan Unit Gawat Darurat (UGD)',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(422,94,'Pelayanan Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(423,94,'Instalasi Rawat Inap',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(424,94,'Pelayanan Program Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(425,94,'Pelayanan Kesehatan Jiwa',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(426,94,'Pelayanan IVA dan Krioteraphy',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(427,94,'Pelayanan POS Pralansia dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(428,94,'Pelayanan Konsultasi Gizi',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(429,94,'Pelayanan UKS/ UKGS',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(430,94,'Pelayanan Prolanis',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(431,94,'Pelayanan TBC dan Kusta',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(432,95,'Pelayanan Kesehatan Ibu dan Anak / KB',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(433,95,'Pelayanan Posyandu Balita dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(434,95,'Pelayanan Unit Gawat Darurat (UGD)',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(435,95,'Pelayanan Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(436,95,'Instalasi Rawat Inap',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(437,95,'Pelayanan Program Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(438,95,'Pelayanan Kesehatan Jiwa',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(439,95,'Pelayanan IVA dan Krioteraphy',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(440,95,'Pelayanan POS Pralansia dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(441,95,'Pelayanan Konsultasi Gizi',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(442,95,'Pelayanan UKS/ UKGS',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(443,95,'Pelayanan Prolanis',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(444,95,'Pelayanan TBC dan Kusta',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(445,96,'Pelayanan Kesehatan Ibu dan Anak / KB',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(446,96,'Pelayanan Posyandu Balita dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(447,96,'Pelayanan Unit Gawat Darurat (UGD)',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(448,96,'Pelayanan Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(449,96,'Instalasi Rawat Inap',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(450,96,'Pelayanan Program Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(451,96,'Pelayanan Kesehatan Jiwa',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(452,96,'Pelayanan IVA dan Krioteraphy',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(453,96,'Pelayanan POS Pralansia dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(454,96,'Pelayanan Konsultasi Gizi',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(455,96,'Pelayanan UKS/ UKGS',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(456,96,'Pelayanan Prolanis',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(457,96,'Pelayanan TBC dan Kusta',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(458,97,'Pelayanan Kesehatan Ibu dan Anak / KB',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(459,97,'Pelayanan Posyandu Balita dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(460,97,'Pelayanan Unit Gawat Darurat (UGD)',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(461,97,'Pelayanan Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(462,97,'Instalasi Rawat Inap',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(463,97,'Pelayanan Program Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(464,97,'Pelayanan Kesehatan Jiwa',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(465,97,'Pelayanan IVA dan Krioteraphy',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(466,97,'Pelayanan POS Pralansia dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(467,97,'Pelayanan Konsultasi Gizi',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(468,97,'Pelayanan UKS/ UKGS',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(469,97,'Pelayanan Prolanis',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(470,97,'Pelayanan TBC dan Kusta',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(471,98,'Pelayanan Kesehatan Ibu dan Anak / KB',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(472,98,'Pelayanan Posyandu Balita dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(473,98,'Pelayanan Unit Gawat Darurat (UGD)',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(474,98,'Pelayanan Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(475,98,'Instalasi Rawat Inap',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(476,98,'Pelayanan Program Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(477,98,'Pelayanan Kesehatan Jiwa',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(478,98,'Pelayanan IVA dan Krioteraphy',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(479,98,'Pelayanan POS Pralansia dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(480,98,'Pelayanan Konsultasi Gizi',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(481,98,'Pelayanan UKS/ UKGS',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(482,98,'Pelayanan Prolanis',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(483,98,'Pelayanan TBC dan Kusta',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(484,99,'Pelayanan Kesehatan Ibu dan Anak / KB',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(485,99,'Pelayanan Posyandu Balita dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(486,99,'Pelayanan Unit Gawat Darurat (UGD)',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(487,99,'Pelayanan Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(488,99,'Instalasi Rawat Inap',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(489,99,'Pelayanan Program Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(490,99,'Pelayanan Kesehatan Jiwa',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(491,99,'Pelayanan IVA dan Krioteraphy',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(492,99,'Pelayanan POS Pralansia dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(493,99,'Pelayanan Konsultasi Gizi',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(494,99,'Pelayanan UKS/ UKGS',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(495,99,'Pelayanan Prolanis',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(496,99,'Pelayanan TBC dan Kusta',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(497,100,'Pelayanan Kesehatan Ibu dan Anak / KB',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(498,100,'Pelayanan Posyandu Balita dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(499,100,'Pelayanan Unit Gawat Darurat (UGD)',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(500,100,'Pelayanan Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(501,100,'Instalasi Rawat Inap',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(502,100,'Pelayanan Program Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(503,100,'Pelayanan Kesehatan Jiwa',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(504,100,'Pelayanan IVA dan Krioteraphy',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(505,100,'Pelayanan POS Pralansia dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(506,100,'Pelayanan Konsultasi Gizi',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(507,100,'Pelayanan UKS/ UKGS',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(508,100,'Pelayanan Prolanis',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(509,100,'Pelayanan TBC dan Kusta',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(510,101,'Pelayanan Kesehatan Ibu dan Anak / KB',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(511,101,'Pelayanan Posyandu Balita dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(512,101,'Pelayanan Unit Gawat Darurat (UGD)',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(513,101,'Pelayanan Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(514,101,'Instalasi Rawat Inap',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(515,101,'Pelayanan Program Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(516,101,'Pelayanan Kesehatan Jiwa',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(517,101,'Pelayanan IVA dan Krioteraphy',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(518,101,'Pelayanan POS Pralansia dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(519,101,'Pelayanan Konsultasi Gizi',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(520,101,'Pelayanan UKS/ UKGS',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(521,101,'Pelayanan Prolanis',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(522,101,'Pelayanan TBC dan Kusta',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(523,102,'Pelayanan Kesehatan Ibu dan Anak / KB',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(524,102,'Pelayanan Posyandu Balita dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(525,102,'Pelayanan Unit Gawat Darurat (UGD)',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(526,102,'Pelayanan Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(527,102,'Instalasi Rawat Inap',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(528,102,'Pelayanan Program Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(529,102,'Pelayanan Kesehatan Jiwa',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(530,102,'Pelayanan IVA dan Krioteraphy',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(531,102,'Pelayanan POS Pralansia dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(532,102,'Pelayanan Konsultasi Gizi',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(533,102,'Pelayanan UKS/ UKGS',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(534,102,'Pelayanan Prolanis',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(535,102,'Pelayanan TBC dan Kusta',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(536,103,'Pelayanan Kesehatan Ibu dan Anak / KB',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(537,103,'Pelayanan Posyandu Balita dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(538,103,'Pelayanan Unit Gawat Darurat (UGD)',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(539,103,'Pelayanan Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(540,103,'Instalasi Rawat Inap',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(541,103,'Pelayanan Program Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(542,103,'Pelayanan Kesehatan Jiwa',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(543,103,'Pelayanan IVA dan Krioteraphy',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(544,103,'Pelayanan POS Pralansia dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(545,103,'Pelayanan Konsultasi Gizi',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(546,103,'Pelayanan UKS/ UKGS',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(547,103,'Pelayanan Prolanis',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(548,103,'Pelayanan TBC dan Kusta',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(549,104,'Pelayanan Kesehatan Ibu dan Anak / KB',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(550,104,'Pelayanan Posyandu Balita dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(551,104,'Pelayanan Unit Gawat Darurat (UGD)',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(552,104,'Pelayanan Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(553,104,'Instalasi Rawat Inap',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(554,104,'Pelayanan Program Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(555,104,'Pelayanan Kesehatan Jiwa',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(556,104,'Pelayanan IVA dan Krioteraphy',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(557,104,'Pelayanan POS Pralansia dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(558,104,'Pelayanan Konsultasi Gizi',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(559,104,'Pelayanan UKS/ UKGS',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(560,104,'Pelayanan Prolanis',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(561,104,'Pelayanan TBC dan Kusta',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(562,105,'Pelayanan Kesehatan Ibu dan Anak / KB',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(563,105,'Pelayanan Posyandu Balita dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(564,105,'Pelayanan Unit Gawat Darurat (UGD)',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(565,105,'Pelayanan Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(566,105,'Instalasi Rawat Inap',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(567,105,'Pelayanan Program Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(568,105,'Pelayanan Kesehatan Jiwa',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(569,105,'Pelayanan IVA dan Krioteraphy',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(570,105,'Pelayanan POS Pralansia dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(571,105,'Pelayanan Konsultasi Gizi',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(572,105,'Pelayanan UKS/ UKGS',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(573,105,'Pelayanan Prolanis',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(574,105,'Pelayanan TBC dan Kusta',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(575,106,'Pelayanan Kesehatan Ibu dan Anak / KB',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(576,106,'Pelayanan Posyandu Balita dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(577,106,'Pelayanan Unit Gawat Darurat (UGD)',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(578,106,'Pelayanan Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(579,106,'Instalasi Rawat Inap',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(580,106,'Pelayanan Program Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(581,106,'Pelayanan Kesehatan Jiwa',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(582,106,'Pelayanan IVA dan Krioteraphy',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(583,106,'Pelayanan POS Pralansia dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(584,106,'Pelayanan Konsultasi Gizi',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(585,106,'Pelayanan UKS/ UKGS',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(586,106,'Pelayanan Prolanis',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(587,106,'Pelayanan TBC dan Kusta',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(588,107,'Pelayanan Kesehatan Ibu dan Anak / KB',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(589,107,'Pelayanan Posyandu Balita dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(590,107,'Pelayanan Unit Gawat Darurat (UGD)',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(591,107,'Pelayanan Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(592,107,'Instalasi Rawat Inap',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(593,107,'Pelayanan Program Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(594,107,'Pelayanan Kesehatan Jiwa',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(595,107,'Pelayanan IVA dan Krioteraphy',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(596,107,'Pelayanan POS Pralansia dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(597,107,'Pelayanan Konsultasi Gizi',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(598,107,'Pelayanan UKS/ UKGS',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(599,107,'Pelayanan Prolanis',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(600,107,'Pelayanan TBC dan Kusta',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(601,108,'Pelayanan Kesehatan Ibu dan Anak / KB',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(602,108,'Pelayanan Posyandu Balita dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(603,108,'Pelayanan Unit Gawat Darurat (UGD)',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(604,108,'Pelayanan Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(605,108,'Instalasi Rawat Inap',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(606,108,'Pelayanan Program Kesehatan',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(607,108,'Pelayanan Kesehatan Jiwa',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(608,108,'Pelayanan IVA dan Krioteraphy',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(609,108,'Pelayanan POS Pralansia dan Lansia',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(610,108,'Pelayanan Konsultasi Gizi',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(611,108,'Pelayanan UKS/ UKGS',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(612,108,'Pelayanan Prolanis',1,'admin','2021-05-18 18:00:00',NULL,NULL),
	(613,108,'Pelayanan TBC dan Kusta',1,'admin','2021-05-18 18:00:00',NULL,NULL);

/*!40000 ALTER TABLE `skm_mlayanan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table skm_mopd
# ------------------------------------------------------------

DROP TABLE IF EXISTS `skm_mopd`;

CREATE TABLE `skm_mopd` (
  `IdOPD` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `NmOPD` varchar(200) NOT NULL DEFAULT '',
  `IsAktif` tinyint(1) NOT NULL DEFAULT '1',
  `CreatedBy` varchar(50) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`IdOPD`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `skm_mopd` WRITE;
/*!40000 ALTER TABLE `skm_mopd` DISABLE KEYS */;

INSERT INTO `skm_mopd` (`IdOPD`, `NmOPD`, `IsAktif`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(42,'Badan Kepegawaian Daerah',1,NULL,NULL,NULL,NULL),
	(43,'Badan Kesatuan Bangsa dan Politik',1,NULL,NULL,NULL,NULL),
	(44,'Badan Penanggulangan Bencana Daerah',1,NULL,NULL,NULL,NULL),
	(45,'Badan Pengelolaan Keuangan dan Asset Daerah',1,NULL,NULL,NULL,NULL),
	(46,'Badan Pengelolaan Pajak dan Retribusi Daerah',1,NULL,NULL,NULL,NULL),
	(47,'Badan Perencanaan Pembangunan Daerah',1,NULL,NULL,NULL,NULL),
	(48,'Dinas Kepemudaan, Olahraga dan Pariwisata',1,NULL,NULL,NULL,NULL),
	(49,'Dinas Kependudukan dan Pencatatan Sipil',1,NULL,NULL,NULL,NULL),
	(50,'Dinas Kesehatan',1,NULL,NULL,NULL,NULL),
	(51,'Dinas Ketenagakerjaan',1,NULL,NULL,NULL,NULL),
	(52,'Dinas Komunikasi dan Informatika',1,NULL,NULL,NULL,NULL),
	(53,'Dinas Koperasi, Usaha Kecil dan Menengah',1,NULL,NULL,NULL,NULL),
	(54,'Dinas Lingkungan Hidup, Kebersihan dan Pertamanan',1,NULL,NULL,NULL,NULL),
	(55,'Dinas Pekerjaan Umum dan Penataan Ruang',1,NULL,NULL,NULL,NULL),
	(56,'Dinas Pemberdayaan Masyarakat dan Desa',1,NULL,NULL,NULL,NULL),
	(57,'Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu',1,NULL,NULL,NULL,NULL),
	(58,'Dinas Pendidikan',1,NULL,NULL,NULL,NULL),
	(59,'Dinas Pengendalian Penduduk, Keluarga Berencana, Pemberdayaan Perempuan dan Perlindungan Anak',1,NULL,NULL,NULL,NULL),
	(60,'Dinas Perhubungan',1,NULL,NULL,NULL,NULL),
	(61,'Dinas Perikanan',1,NULL,NULL,NULL,NULL),
	(62,'Dinas Perindustrian dan Perdagangan',1,NULL,NULL,NULL,NULL),
	(63,'Dinas Perpustakaan',1,NULL,NULL,NULL,NULL),
	(64,'Dinas Perumahan dan Kawasan Permukiman',1,NULL,NULL,NULL,NULL),
	(65,'Dinas Peternakan dan Perkebunan',1,NULL,NULL,NULL,NULL),
	(66,'Dinas Sosial',1,NULL,NULL,NULL,NULL),
	(67,'Dinas Tanaman Pangan, Hortikultura dan Ketahanan Pangan',1,NULL,NULL,NULL,NULL),
	(68,'Inspektorat',1,NULL,NULL,NULL,NULL),
	(69,'Kecamatan Air Putih',1,NULL,NULL,NULL,NULL),
	(70,'Kecamatan Datuk Lima Puluh',1,NULL,NULL,NULL,NULL),
	(71,'Kecamatan Datuk Tanah Datar',1,NULL,NULL,NULL,NULL),
	(72,'Kecamatan Laut Tador',1,NULL,NULL,NULL,NULL),
	(73,'Kecamatan Lima Puluh',1,NULL,NULL,NULL,NULL),
	(74,'Kecamatan Lima Puluh Pesisir',1,NULL,NULL,NULL,NULL),
	(75,'Kecamatan Medang Deras',1,NULL,NULL,NULL,NULL),
	(76,'Kecamatan Nibung Hangus',1,NULL,NULL,NULL,NULL),
	(77,'Kecamatan Sei Balai',1,NULL,NULL,NULL,NULL),
	(78,'Kecamatan Sei Suka',1,NULL,NULL,NULL,NULL),
	(79,'Kecamatan Talawi',1,NULL,NULL,NULL,NULL),
	(80,'Kecamatan Tanjung Tiram',1,NULL,NULL,NULL,NULL),
	(81,'Satuan Polisi Pamong Praja',1,NULL,NULL,NULL,NULL),
	(82,'Sekretariat DPRD',1,NULL,NULL,NULL,NULL),
	(83,'Sekretariat Daerah',1,NULL,NULL,NULL,NULL),
	(84,'Bagian Organisasi',1,NULL,NULL,NULL,NULL),
	(85,'Bagian Kesejahteraan Rakyat',1,NULL,NULL,NULL,NULL),
	(86,'Bagian Perencanaan dan Keuangan',1,NULL,NULL,NULL,NULL),
	(87,'Bagian Tata Pemerintahan',1,NULL,NULL,NULL,NULL),
	(88,'Bagian Umum',1,NULL,NULL,NULL,NULL),
	(89,'Bagian Perekonomian dan SDA',1,NULL,NULL,NULL,NULL),
	(90,'Bagian Pengadaan Barang dan Jasa',1,NULL,NULL,NULL,NULL),
	(91,'Bagian Administrasi Pembangunan',1,NULL,NULL,NULL,NULL),
	(92,'Bagian Protokol dan Komunikasi Pimpinan',1,NULL,NULL,NULL,NULL),
	(93,'Bagian Hukum',1,NULL,NULL,NULL,NULL),
	(94,'Puskesmas Pagurawan',1,'admin','2021-05-18 18:44:41',NULL,NULL),
	(95,'Puskesmas Lalang',1,'admin','2021-05-18 18:44:49',NULL,NULL),
	(96,'Puskesmas Sei Suka',1,'admin','2021-05-18 18:44:58',NULL,NULL),
	(97,'Puskesmas Indrapura',1,'admin','2021-05-18 18:45:08',NULL,NULL),
	(98,'Puskesmas Pematang Panjang',1,'admin','2021-05-18 18:45:19',NULL,NULL),
	(99,'Puskesmas Petatal',1,'admin','2021-05-18 18:45:26',NULL,NULL),
	(100,'Puskesmas Sei Bejangkar',1,'admin','2021-05-18 18:45:34',NULL,NULL),
	(101,'Puskesmas Sei Balai',1,'admin','2021-05-18 18:45:42',NULL,NULL),
	(102,'Puskesmas Tanjung Tiram',1,'admin','2021-05-18 18:45:51',NULL,NULL),
	(103,'Puskesmas Ujung Kubu',1,'admin','2021-05-18 18:45:58',NULL,NULL),
	(104,'Puskesmas Kedai Sianam',1,'admin','2021-05-18 18:46:24',NULL,NULL),
	(105,'Puskesmas Laut Tador',1,'admin','2021-05-18 18:46:33',NULL,NULL),
	(106,'Puskesmas Labuhan Ruku',1,'admin','2021-05-18 18:46:42',NULL,NULL),
	(107,'Puskesmas Lima Puluh',1,'admin','2021-05-18 18:46:51',NULL,NULL),
	(108,'Puskesmas Simpang Dolok',1,'admin','2021-05-18 18:46:58',NULL,NULL);

/*!40000 ALTER TABLE `skm_mopd` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table skm_tsurvey
# ------------------------------------------------------------

DROP TABLE IF EXISTS `skm_tsurvey`;

CREATE TABLE `skm_tsurvey` (
  `IdSurvey` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdOPD` bigint(10) unsigned NOT NULL,
  `IdLayanan` bigint(10) unsigned NOT NULL,
  `NmReferensi` varchar(200) DEFAULT NULL,
  `NmJenisKelamin` varchar(1) NOT NULL DEFAULT '',
  `Umur` double NOT NULL,
  `NmPendidikan` varchar(200) NOT NULL DEFAULT '',
  `NmPekerjaan` varchar(200) NOT NULL DEFAULT '',
  `Timestamp` datetime NOT NULL,
  `Remarks` text NOT NULL,
  PRIMARY KEY (`IdSurvey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table skm_tsurveyskor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `skm_tsurveyskor`;

CREATE TABLE `skm_tsurveyskor` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdSurvey` bigint(10) unsigned NOT NULL,
  `IdKuisioner` bigint(10) unsigned NOT NULL,
  `NmKuisionerJudul` text,
  `NmKuisioner` text NOT NULL,
  `NmOpt` text NOT NULL,
  `Skor` double NOT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
