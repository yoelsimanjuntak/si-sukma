<?php
$carouselDir =  scandir(MY_IMAGEPATH.'slide/');
$carouselArr = array();
foreach (scandir(MY_IMAGEPATH.'slide/') as $file) {
  if(strpos(mime_content_type(MY_IMAGEPATH.'slide/'.$file), 'image') !== false) {;
    $carouselArr[MY_IMAGEURL.'slide/'.$file] = filemtime(MY_IMAGEPATH.'slide/'. $file); //$carouselArr[] = MY_IMAGEURL.'slide/'.$file;
  }
}
arsort($carouselArr);
$carouselArr = array_keys($carouselArr);

$getOPD = null;
$ropd = array();

if(isset($_GET['idOPD'])) {
  $getOPD = $_GET['idOPD'];
}
if(isset($idOPD)) {
  $getOPD = $idOPD;
}
if(!empty($getOPD)) {
  $ropd = $this->db->where(COL_IDOPD, $getOPD)->get(TBL_SKM_MOPD)->row_array();
}
?>
<div class="content-wrapper">
  <div class="content pt-3">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 d-none d-sm-inline-block">
          <div class="card animated bounceInLeft">
            <div class="card-body p-0">
              <?php
              if(!empty($carouselArr)) {
                ?>
                <div id="carouselMain" class="carousel slide carousel-fade" data-ride="carousel">
                  <ol class="carousel-indicators">
                    <?php
                    for($i=0; $i<count($carouselArr); $i++) {
                      ?>
                      <li data-target="#carouselMain" data-slide-to="<?=$i?>" <?=$i==0?'class="active"':''?>></li>
                      <?php
                    }
                    ?>
                  </ol>
                  <div class="carousel-inner">
                    <?php
                    for($i=0; $i<count($carouselArr); $i++) {
                      ?>
                      <div class="carousel-item <?=$i==0?'active':''?>">
                        <!--<img class="d-block" src="<?=$carouselArr[$i]?>" style="height: 200px">-->
                        <div class="d-block w-100" style="background: url('<?=$carouselArr[$i]?>'); background-size: cover; background-repeat: no-repeat; height: 28vh">
                        </div>
                      </div>
                      <?php
                    }
                    ?>
                  </div>
                  <a class="carousel-control-prev" href="#carouselMain" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselMain" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
                <?php
              }
              ?>
              <div class="row">
                <div class="col-lg-12">
                  <div class="p-3">
                    <?=$this->setting_web_welcomemsg?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div id="kuisioner-start" class="card card-widget widget-user animated bounceInRight">
            <div class="widget-user-header bg-info">
              <h3 class="widget-user-username"><?=$this->setting_web_name?></h3>
              <h6 class="widget-user-desc">
                <?=!empty($ropd)?$ropd[COL_NMOPD]:$this->setting_web_desc?>
              </h6>
            </div>
            <div class="widget-user-image bg-white">
              <img class="elevation-1" src="<?=MY_IMAGEURL.$this->setting_web_logo?>" alt="Logo" style="border: 3px solid #00bcd4 !important">
            </div>
            <div class="card-body pt-5">
              <div class="row">
                <div class="col-lg-12">
                  <p class="text-center">
                    Survei ini berisi kuisioner yang disusun sebagai media penyampaian penilaian dari Bapak / Ibu / Saudara.
                    Silakan klik tombol <strong>MULAI</strong> dibawah untuk mengisi kuisioner.
                  </p>
                </div>
              </div>
            </div>
            <div class="card-footer pt-0 pb-0">
              <div class="row">
                <div class="col-lg-12 pt-3">
                  <div class="callout callout-danger">
                    <p class="text-sm">
                      <strong>CATATAN:</strong><br />
                      Mohon setiap pertanyaan dijawab dengan benar dan jujur. Kami menjamin kerahasiaan atas informasi yang Bapak/Ibu/Saudara berikan.
                    </p>
                  </div>
                  <p>
                    <button type="button" id="btn-start" class="btn btn-outline-info btn-block elevation-1"><i class="far fa-flag-checkered"></i>&nbsp;MULAI!</button>
                  </p>
                </div>
              </div>
              <div class="row bg-white" style="margin: 0 -20px !important">
                <div class="col-sm-12">
                  <p class="text-sm mb-0 font-italic">
                    <span class="pull-right">
                      atau <a href="<?=site_url('admin')?>">masuk sebagai <strong>ADMIN</strong>&nbsp;<i class="far fa-arrow-right"></i></a>
                    </span>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div id="kuisioner-finish" class="card card-widget widget-user d-none">
            <div class="widget-user-header bg-info">
              <h3 class="widget-user-username">SURVEI SELESAI</h3>
              <h6 class="widget-user-desc"><?=$this->setting_web_desc?></h6>
            </div>
            <div class="widget-user-image bg-white">
              <img class="elevation-1" src="<?=MY_IMAGEURL.$this->setting_web_logo?>" alt="Logo" style="border: 3px solid #00bcd4 !important">
            </div>
            <div class="card-body pt-5">
              <div class="row">
                <div class="col-lg-12">
                  <p class="text-center">
                    <strong>TERIMA KASIH</strong> telah berpartisipasi dalam mengisi survei!<br />
                    Survei yang anda isi sangat berarti dalam meningkatkan mutu pelayanan publik oleh Pemerintah Kabupaten Batu Bara
                  </p>
                </div>
              </div>
            </div>
            <div class="card-footer pt-0">
              <div class="row">
                <div class="col-lg-12 pt-3">
                  <p class="mb-0">
                    <a href="<?=site_url()?>" class="btn btn-outline-info btn-block elevation-1"><i class="far fa-refresh"></i>&nbsp;KEMBALI KE AWAL</a>
                    <!--<button type="button" class="btn btn-outline-warning btn-block elevation-1 btn-reload"><i class="far fa-refresh"></i>&nbsp;ISI ULANG</button>-->
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div id="kuisioner-0" class="card d-none">
            <div class="card-header">
              <h5 class="card-title font-weight-bold">IDENTITAS & JENIS LAYANAN</h5>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-3 p-2">
                  <div class="form-group">
                    <label>No. Buku Tamu</label>
                    <input type="text" class="form-control" name="<?=COL_NMREFERENSI?>" />
                  </div>
                </div>
                <div class="col-sm-3 p-2">
                  <div class="form-group">
                    <label>Umur</label>
                    <input type="number" class="form-control text-right" name="<?=COL_UMUR?>" />
                  </div>
                </div>
                <div class="col-sm-4 p-2">
                  <div class="form-group">
                    <label>Jenis Kelamin</label>
                    <div class="row">
                      <div class="col-sm-7">
                        <div class="form-check mt-2">
                          <input class="form-check-input" type="radio" name="<?=COL_NMJENISKELAMIN?>" id="chkJKPria" value="LAKI-LAKI" checked="">
                          <label class="form-check-label" for="chkJKPria">LAKI-LAKI</label>
                        </div>
                      </div>
                      <div class="col-sm-5">
                        <div class="form-check mt-2">
                          <input class="form-check-input" type="radio" name="<?=COL_NMJENISKELAMIN?>" id="chkJKWanita" value="PEREMPUAN" >
                          <label class="form-check-label" for="chkJKWanita">PEREMPUAN</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6 p-2">
                  <div class="form-group">
                    <label>Pendidikan Terakhir</label>
                    <select name="<?=COL_NMPENDIDIKAN?>" class="form-control" style="width: 100%">
                      <option value="SD">SD</option>
                      <option value="SLTP">SLTP</option>
                      <option value="SLTA">SLTA</option>
                      <option value="D1/D3/D4">D1/D3/D4</option>
                      <option value="S1">S1</option>
                      <option value="S2">S2</option>
                      <option value=">S2">>S2</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6 p-2">
                  <div class="form-group">
                    <label>Pekerjaan</label>
                    <select name="<?=COL_NMPEKERJAAN?>" class="form-control" style="width: 100%">
                      <option value="PNS/TNI/POLRI">PNS/TNI/POLRI</option>
                      <option value="PEGAWAI SWASTA">PEGAWAI SWASTA</option>
                      <option value="WIRAUSAHAWAN">WIRAUSAHAWAN</option>
                      <option value="PELAJAR/MAHASISWA">PELAJAR/MAHASISWA</option>
                      <option value="LAINNYA">LAINNYA</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="form-group">
                    <label>OPD / Instansi Penyelenggara</label>
                    <?php
                    if(!empty($ropd)) {
                      ?>
                      <input type="hidden" name="<?=COL_IDOPD?>" value="<?=$ropd[COL_IDOPD]?>"/>
                      <input type="text" class="form-control" value="<?=$ropd[COL_NMOPD]?>" readonly/>
                      <?php
                    } else {
                      ?>
                      <select class="form-control" name="<?=COL_IDOPD?>" style="width: 100%">
                        <?=GetCombobox("SELECT * FROM skm_mopd ORDER BY NmOPD", COL_IDOPD, COL_NMOPD)?>
                      </select>
                      <?php
                    }
                    ?>

                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="form-group">
                    <label>Layanan</label>
                    <select class="form-control" name="<?=COL_IDLAYANAN?>" style="width: 100%">
                      <option value="">-- Pilih Layanan --</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-footer">
              <button class="btn btn-outline-danger pull-left btn-cancel elevation-1"><i class="far fa-times"></i>&nbsp;BATAL</button>
              <button class="btn btn-outline-info pull-right btn-next elevation-1">SELANJUTNYA&nbsp;<i class="far fa-chevron-right"></i></button>
            </div>
          </div>
          <?php
          $rkuisioner = $this->db
          ->where(COL_ISAKTIF,1)
          ->order_by(COL_SEQ, 'asc')
          ->get(TBL_SKM_MKUISIONER)
          ->result_array();
          $i=1;
          foreach($rkuisioner as $k) {
            ?>
            <div id="kuisioner-<?=$i?>" class="card d-none">
              <div class="card-header no-border">
                <h5 class="card-title"><?='['.$i.'/'.(count($rkuisioner)+1).'] '.$k[COL_NMKUISIONERJUDUL]?></h5>
              </div>
              <div class="progress progress-xs">
                  <div class="progress-bar bg-info progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="<?=$i/(count($rkuisioner)+1)*100?>" aria-valuemin="0" aria-valuemax="100" style="width: <?=$i/(count($rkuisioner)+1)*100?>%">
                </div>
              </div>
              <div class="card-body">
                <p class="font-weight-bold"><?=$k[COL_NMKUISIONER]?></p>
                <div class="row">
                  <div class="col-sm-12">
                    <?php
                    $ropt = $this->db
                    ->where(COL_IDKUISIONER, $k[COL_IDKUISIONER])
                    ->order_by(COL_SKOR, 'asc')
                    ->get(TBL_SKM_MKUISIONEROPT)
                    ->result_array();

                    foreach($ropt as $opt) {
                      ?>
                      <div class="form-check mt-2">
                        <input class="form-check-input" type="radio" name="Q[<?=$i?>]" id="Q-<?=$k[COL_IDKUISIONER]?>-<?=$opt[COL_IDOPT]?>" data-idkuisioner="<?=$k[COL_IDKUISIONER]?>" value="<?=$opt[COL_IDOPT]?>">
                        <label class="form-check-label" for="Q-<?=$k[COL_IDKUISIONER]?>-<?=$opt[COL_IDOPT]?>"><?=$opt[COL_NMOPT]?></label>
                      </div>
                      <?php
                    }
                    ?>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <button class="btn btn-outline-secondary pull-left btn-prev"><i class="far fa-chevron-left"></i>&nbsp;SEBELUMNYA</button>
                <button class="btn btn-outline-info pull-right btn-next elevation-1">SELANJUTNYA&nbsp;<i class="far fa-chevron-right"></i></button>
              </div>
            </div>
            <?php
            $i++;
          }
          ?>
          <div id="kuisioner-<?=$i?>" class="card d-none">
            <div class="card-header no-border">
              <h5 class="card-title"><?='['.$i.'/'.(count($rkuisioner)+1).'] SARAN / MASUKAN (OPSIONAL)'?></h5>
            </div>
            <div class="progress progress-xs">
                <div class="progress-bar bg-info progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="<?=$i/(count($rkuisioner)+1)*100?>" aria-valuemin="0" aria-valuemax="100" style="width: <?=$i/(count($rkuisioner)+1)*100?>%">
              </div>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label>Silakan masukkan saran / masukan Bapak / Ibu / Saudara sebagai bahan evaluasi untuk meningkatkan mutu pelayanan.</label>
                <textarea name="<?=COL_NMSARAN?>" class="form-control" rows="5"></textarea>
              </div>
            </div>
            <div class="card-footer">
              <button class="btn btn-outline-secondary pull-left btn-prev"><i class="far fa-chevron-left"></i>&nbsp;SEBELUMNYA</button>
              <button class="btn btn-outline-success pull-right btn-finish elevation-1"><i class="far fa-check-square"></i>&nbsp;SELESAI</button>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
function validateKuisioner(currEl) {
  var chkboxEl = $('input[type=radio]', currEl);
  var selectEl = $('select', currEl);
  var textEl = $('input[type=text],input[type=number],textarea', currEl);
  var ok = true;

  $.each(chkboxEl, function(idx, obj) {
    var nameEl = $(obj)[0].name;
    var checkedEl = $('[name="'+nameEl+'"]:checked');
    if(!checkedEl || checkedEl.length == 0) {
      ok = false;
    }
  });
  $.each(selectEl, function(idx, obj) {
    var val = $(obj).val();
    if(!val) {
      ok = false;
    }
  });
  $.each(textEl, function(idx, obj) {
    var val = $(obj).val();
    if(!val) {
      ok = false;
    }
  });
  return ok;
}
$(document).ready(function(){
  $('#btn-start').click(function(){
    $('#kuisioner-start').removeClass('animated bounceInRight').addClass('animated fadeOutLeft');
    $('#kuisioner-0').removeClass('d-none');
    $('#kuisioner-0').removeClass('animated');
    $('#kuisioner-0').removeClass('fadeOutLeft');
    $('#kuisioner-0').addClass('animated bounceInRight');
    $('#kuisioner-start').addClass('d-none');
  });
  $('.btn-next').click(function(){
    var currEl = $(this).closest('.card');
    /* validation */
    var ok = validateKuisioner(currEl);
    if(!ok) {
      toastr.error('Mohon lengkapi isian anda terlebih dahulu.');
      return false;
    }
    /* validation */

    if(currEl.length > 0) {
      var currQuest = currEl[0].id.split('kuisioner-').pop();
      if(currQuest>=0) {
        var nextEl = $('#kuisioner-'+(parseInt(currQuest)+1));
        $(currEl[0]).removeClass('bounceInRight').addClass('animated fadeOutLeft');
        nextEl.removeClass('d-none');
        nextEl.removeClass('animated');
        nextEl.removeClass('fadeOutLeft');
        nextEl.addClass('animated bounceInRight');
        $(currEl[0]).addClass('d-none');
      }
    }
  });
  $('.btn-prev').click(function(){
    var currEl = $(this).closest('.card');
    if(currEl.length > 0) {
      var currQuest = currEl[0].id.split('kuisioner-').pop();
      if(currQuest>=0) {
        var prevEl = $('#kuisioner-'+(parseInt(currQuest)-1));
        $(currEl[0]).removeClass('fadeInLeft').addClass('animated fadeOutLeft');
        prevEl.removeClass('d-none');
        prevEl.removeClass('animated');
        prevEl.removeClass('fadeOutLeft');
        prevEl.addClass('animated fadeInLeft');
        $(currEl[0]).addClass('d-none');
      }
    }
  });
  $('.btn-cancel').click(function(){
    if(confirm('Apakah anda yakin ingin membatalkan survei ini?')) {
      location.reload();
    }
  });
  $('.btn-reload').click(function() {
    var startEl = $('#kuisioner-0');
    var currEl = $(this).closest('.card');
    if(currEl.length>=0) {
      $(currEl[0]).removeClass('bounceInRight').addClass('animated fadeOutLeft');
      startEl.removeClass('d-none');
      startEl.removeClass('animated');
      startEl.removeClass('fadeOutLeft');
      startEl.addClass('animated flipInY');
      $(currEl[0]).addClass('d-none');
    }

  });
  $('.btn-finish').click(function(){
    var currEl = $(this).closest('.card');
    /* validation */
    var ok = validateKuisioner(currEl);
    if(!ok) {
      toastr.error('Mohon lengkapi isian anda terlebih dahulu.');
      return false;
    }
    /* validation */

    var kuisionerArr = [];
    for(var i=1; i<=<?=count($rkuisioner)?>; i++) {
      var id_ = $('[name="Q['+i+']"]').data('idkuisioner');
      var op_ = $('[name="Q['+i+']"]:checked').val();

      kuisionerArr.push({IdKuisioner: id_, IdOpt: op_});
    }

    var surveiObj = {
      NmReferensi: $('[name=NmReferensi]').val(),
      Umur: $('[name=Umur]').val(),
      NmJenisKelamin: $('[name=NmJenisKelamin]:checked').val(),
      NmPendidikan: $('[name=NmPendidikan]').val(),
      NmPekerjaan: $('[name=NmPekerjaan]').val(),
      NmSaran: $('[name=NmSaran]').val(),
      IdOPD: $('[name=IdOPD]').val(),
      IdLayanan: $('[name=IdLayanan]').val(),
      Kuisioner: kuisionerArr
    }
    $.post('<?=site_url('site/ajax/save-survei')?>', surveiObj, function(res) {
      res = JSON.parse(res);
      if(res.error==0) {
        //toastr.success(res.success);

        var thankyouEl = $('#kuisioner-finish');
        $(currEl[0]).removeClass('bounceInRight').addClass('animated fadeOutLeft');
        thankyouEl.removeClass('d-none');
        thankyouEl.removeClass('animated');
        thankyouEl.removeClass('fadeOutLeft');
        thankyouEl.addClass('animated flipInY');
        $(currEl[0]).addClass('d-none');

      } else {
        toastr.error(res.error);
        setTimeout(function(){
          location.reload();
        }, 2000);
      }
    }).fail(function() {
      toastr.error('Mohon maaf, telah terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi.');
      setTimeout(function(){
        location.reload();
      }, 2000);
    });

  });

  $('[name=IdOPD]').change(function() {
    var idOPD = $(this).val();
    $('[name=IdLayanan]').load('<?=site_url('site/ajax/get-opt-layanan')?>', {idOPD: idOPD}, function(){

    });
  }).trigger('change');
});
</script>
