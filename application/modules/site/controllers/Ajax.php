<?php
class Ajax extends MY_Controller {

  public function __construct()
  {
      parent::__construct();
      setlocale (LC_TIME, 'id_ID');
  }

  public function login() {
    $this->form_validation->set_rules(array(
      array(
        'field' => 'UserName',
        'label' => 'UserName',
        'rules' => 'required'
      ),
      array(
        'field' => 'Password',
        'label' => 'Password',
        'rules' => 'required'
      )
    ));
    if($this->form_validation->run()) {
      $this->load->model('muser');
      $username = $this->input->post(COL_USERNAME);
      $password = $this->input->post(COL_PASSWORD);

      if($this->muser->authenticate($username, $password)) {
        if($this->muser->IsSuspend($username)) {
          ShowJsonError('Akun anda di suspend.');
          return;
        }

        $userdetails = $this->muser->getdetails($username);
        /*if(empty($userdetails[COL_IS_EMAILVERIFIED]) || $userdetails[COL_IS_EMAILVERIFIED] != 1) {
          ShowJsonError('Maaf, akun kamu belum diverifikasi. Silakan periksa link verifikasi yang dikirimkan melalui email kamu.');
          return;
        }*/

        $this->db->where(COL_USERNAME, $username);
        $this->db->update(TBL__USERS, array(COL_LASTLOGIN=>date('Y-m-d H:i:s'), COL_LASTLOGINIP=>$this->input->ip_address()));
        SetLoginSession($userdetails);
        ShowJsonSuccess('Login berhasil.', array('redirect'=>site_url('admin/dashboard')));
      } else {
        ShowJsonError('Username / password tidak tepat.');
      }
    }
  }

  public function get_opt_layanan() {
    $idOPD = $this->input->post('idOPD');
    echo GetCombobox("SELECT * FROM skm_mlayanan where IdOPD = $idOPD ORDER BY NmLayanan", COL_IDLAYANAN, COL_NMLAYANAN);
  }

  public function save_survei() {
    $ipaddr = '';
    if(!empty($_SERVER['HTTP_CLIENT_IP'])) {
      $ipaddr = $_SERVER['HTTP_CLIENT_IP'];
    } else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
      $ipaddr = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
      $ipaddr = $_SERVER['REMOTE_ADDR'];
    }

    $arrKuisioner = $this->input->post('Kuisioner');
    $datSurvei = array(
      COL_IDOPD=>$this->input->post(COL_IDOPD),
      COL_IDLAYANAN=>$this->input->post(COL_IDLAYANAN),
      COL_NMREFERENSI=>$this->input->post(COL_NMREFERENSI),
      COL_NMJENISKELAMIN=>$this->input->post(COL_NMJENISKELAMIN),
      COL_UMUR=>$this->input->post(COL_UMUR),
      COL_NMPENDIDIKAN=>$this->input->post(COL_NMPENDIDIKAN),
      COL_NMPEKERJAAN=>$this->input->post(COL_NMPEKERJAAN),
      COL_NMSARAN=>$this->input->post(COL_NMSARAN),
      COL_TIMESTAMP=>date('Y-m-d H:i:s'),
      COL_REMARKS=>$ipaddr.' - '.$_SERVER['HTTP_USER_AGENT']
    );
    $datSurveiSkor = array();

    $this->db->trans_begin();
    try {
      $res = $this->db->insert(TBL_SKM_TSURVEY, $datSurvei);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $idSurvei = $this->db->insert_id();
      if(!empty($arrKuisioner)) {
        foreach($arrKuisioner as $k) {
          $rkuisioner = $this->db
          ->where(COL_IDKUISIONER, $k[COL_IDKUISIONER])
          ->get(TBL_SKM_MKUISIONER)
          ->row_array();

          $ropt = $this->db
          ->where(COL_IDKUISIONER, $k[COL_IDKUISIONER])
          ->where(COL_IDOPT, $k[COL_IDOPT])
          ->get(TBL_SKM_MKUISIONEROPT)
          ->row_array();


          if(!empty($rkuisioner) && !empty($ropt)) {
            $datSurveiSkor[] = array(
              COL_IDSURVEY=>$idSurvei,
              COL_IDKUISIONER=>$k[COL_IDKUISIONER],
              COL_NMKUISIONERJUDUL=>$rkuisioner[COL_NMKUISIONERJUDUL],
              COL_NMKUISIONER=>$rkuisioner[COL_NMKUISIONER],
              COL_NMOPT=>$ropt[COL_NMOPT],
              COL_SKOR=>$ropt[COL_SKOR]
            );
          }
        }
      }

      if(!empty($datSurveiSkor)) {
        $res = $this->db->insert_batch(TBL_SKM_TSURVEYSKOR, $datSurveiSkor);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }
      } else {
        throw new Exception('Kuisioner belum diisi, silakan periksa kembali isian anda.');
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Kuisioner anda BERHASIL dikirim. Terima kasih atas partisipasi anda.');
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
  }
}
