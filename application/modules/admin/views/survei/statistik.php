<?php
$ruser = GetLoggedUser();
function getRandomColor() {
  $letters = explode(",", '0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F');
  $color = '#';
  for($i=0;$i< 6;$i++ ) {
    $color.=$letters[rand(0,15)];
  }
  return $color;
}
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 font-weight-light"><?= $title ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-tachometer-alt"></i> DASHBOARD</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
          <div class="card card-default">
            <div class="card-header">
              <?=form_open(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal','method'=>'get'))?>
              <div class="row">
                <div class="col-sm-12">
                  <?php
                  if($ruser[COL_ROLEID] == ROLEADMIN) {
                    ?>
                    <div class="form-group row">
                      <label class="control-label col-sm-2 text-left mb-0">OPD / INSTANSI</label>
                      <div class="col-sm-5">
                        <select class="form-control " name="idOPD" style="width: 100%">
                          <?=GetCombobox("SELECT * FROM skm_mopd WHERE IsAktif = 1 ORDER BY NmOPD", COL_IDOPD, COL_NMOPD, (!empty($_GET['idOPD'])?$_GET['idOPD']:null), true, false, '-- SEMUA --')?>
                        </select>
                      </div>
                    </div>
                    <?php
                  } else {
                    $ropd = $this->db
                    ->where(COL_IDOPD, $ruser[COL_IDUNIT])
                    ->get(TBL_SKM_MOPD)
                    ->row_array();
                    ?>
                    <div class="form-group row">
                      <label class="control-label col-sm-2 text-left mb-0">OPD / INSTANSI</label>
                      <div class="col-sm-5">
                        <input type="text" class="form-control" value="<?=!empty($ropd)?$ropd[COL_NMOPD]:'--'?>" disabled />
                        <input type="hidden" name="idOPD" value="<?=$ruser[COL_IDUNIT]?>" />
                      </div>
                    </div>

                    <?php
                  }
                  ?>
                  <div class="form-group row">
                    <label class="control-label col-sm-2 text-left mb-0">PERIODE</label>
                    <div class="col-sm-2">
                      <input type="text" class="form-control datepicker text-right" name="filterDateFrom" value="<?=date('Y-m-01')?>" />
                    </div>
                    <label class="control-label col-sm-1 mb-0 text-center">s.d</label>
                    <div class="col-sm-2">
                      <input type="text" class="form-control datepicker text-right" name="filterDateTo" value="<?=date('Y-m-d')?>" />
                    </div>
                  </div>
                  <div class="form-group row" style="margin: 0 -20px !important; border-top: 1px solid #dedede">
                    <div class="col-sm-12 pl-3 mt-3">
                      <button type="submit" class="btn btn-outline-primary" title="Lihat"><i class="fa fa-arrow-circle-right"></i> TAMPILKAN</button>
                    </div>
                  </div>
                </div>
              </div>
              <?=form_close()?>
            </div>
          </div>
      </div>
      <?php
      if(!empty($_GET)) {
        $idOPD = $ruser[COL_ROLEID] == ROLEADMIN ? $_GET['idOPD'] : $ruser[COL_IDUNIT];
        $condOPD = !empty($idOPD)?"IdOPD=$idOPD":"1=1";
        $dateFrom = $_GET['filterDateFrom'];
        $dateTo = $_GET['filterDateTo'];

        $qgender=@"
        select
        UPPER(NmJenisKelamin) as NmJenisKelamin,
        COUNT(*) as COUNT
        from skm_tsurvey
        where
          $condOPD
        	and CAST(skm_tsurvey.Timestamp as DATE) >= '$dateFrom'
        	and CAST(skm_tsurvey.Timestamp as DATE) <= '$dateTo'
        group by UPPER(NmJenisKelamin)
        order by NmJenisKelamin
        ";
        $rgender=$this->db->query($qgender)->result_array();
        $arrBgColor = array();
        $objPieGender = array();
        foreach($rgender as $r) {
          $objPieGender['labels'][] = $r[COL_NMJENISKELAMIN];
          $objPieGender['datasets'][0]['data'][] = $r['COUNT'];
          $objPieGender['datasets'][0]['backgroundColor'][] = getRandomColor();
        }

        $qumur=@"
        select
        Umur,
        COUNT(*) as COUNT
        from skm_tsurvey
        where
          $condOPD
        	and CAST(skm_tsurvey.Timestamp as DATE) >= '$dateFrom'
        	and CAST(skm_tsurvey.Timestamp as DATE) <= '$dateTo'
        group by Umur
        order by Umur
        ";
        $rumur=$this->db->query($qumur)->result_array();
        $arrBgColor = array();
        $objPieUmur = array();
        $objPieUmur['labels'] = array('<= 20','21 - 30', '31 - 40', '41 - 50', '> 51');
        $objPieUmur['datasets'][0]['backgroundColor'] = array('#17a2b8', '#007bff', '#6610f2', '#28a745', '#dc354');
        $objPieUmur['datasets'][0]['data'] = array(0, 0, 0, 0, 0);
        foreach($rumur as $r) {
          if($r[COL_UMUR]<=20) $objPieUmur['datasets'][0]['data'][0] += $r['COUNT'];
          else if($r[COL_UMUR]>20 && $r[COL_UMUR]<=30) $objPieUmur['datasets'][0]['data'][1] += $r['COUNT'];
          else if($r[COL_UMUR]>30 && $r[COL_UMUR]<=40) $objPieUmur['datasets'][0]['data'][2] += $r['COUNT'];
          else if($r[COL_UMUR]>40 && $r[COL_UMUR]<=50) $objPieUmur['datasets'][0]['data'][3] += $r['COUNT'];
          else if($r[COL_UMUR]>50) $objPieUmur['datasets'][0]['data'][4] += $r['COUNT'];
        }

        $qpendidikan=@"
        select
        UPPER(NmPendidikan) as NmPendidikan,
        COUNT(*) as COUNT
        from skm_tsurvey
        where
          $condOPD
        	and CAST(skm_tsurvey.Timestamp as DATE) >= '$dateFrom'
        	and CAST(skm_tsurvey.Timestamp as DATE) <= '$dateTo'
        group by UPPER(NmPendidikan)
        order by NmPendidikan
        ";
        $rpendidikan=$this->db->query($qpendidikan)->result_array();
        $arrBgColor = array();
        $objPiePendidikan = array();
        foreach($rpendidikan as $r) {
          $objPiePendidikan['labels'][] = $r[COL_NMPENDIDIKAN];
          $objPiePendidikan['datasets'][0]['data'][] = $r['COUNT'];
          $objPiePendidikan['datasets'][0]['backgroundColor'][] = getRandomColor();
        }

        $qpekerjaan=@"
        select
        UPPER(NmPekerjaan) as NmPekerjaan,
        COUNT(*) as COUNT
        from skm_tsurvey
        where
          $condOPD
        	and CAST(skm_tsurvey.Timestamp as DATE) >= '$dateFrom'
        	and CAST(skm_tsurvey.Timestamp as DATE) <= '$dateTo'
        group by UPPER(NmPekerjaan)
        order by NmPekerjaan
        ";
        $rpekerjaan=$this->db->query($qpekerjaan)->result_array();
        $arrBgColor = array();
        $objPiePekerjaan = array();
        foreach($rpekerjaan as $r) {
          $objPiePekerjaan['labels'][] = $r[COL_NMPEKERJAAN];
          $objPiePekerjaan['datasets'][0]['data'][] = $r['COUNT'];

          $objPiePekerjaan['datasets'][0]['backgroundColor'][] = getRandomColor();
        }

        $arrBgColor = array();
        $objChartDate = array();
        $objChartDate['datasets'][0]['label'] = 'JLH. RESPONDEN';
        $objChartDate['datasets'][0]['animations'] = array('y'=>array('duration'=>2000, 'delay'=>500));
        $objChartDate['datasets'][0]['backgroundColor'] = '#6610f2';

        $datediff = strtotime($dateTo)-strtotime($dateFrom);
        $dayInterval = round($datediff / (60 * 60 * 24));
        if($dayInterval <= 31) {
          $qdate=@"
          select
          CAST(Timestamp as DATE) as Timestamp,
          COUNT(*) as COUNT
          from skm_tsurvey
          where
            $condOPD
          	and CAST(skm_tsurvey.Timestamp as DATE) >= '$dateFrom'
          	and CAST(skm_tsurvey.Timestamp as DATE) <= '$dateTo'
          group by CAST(Timestamp as DATE)
          order by CAST(Timestamp as DATE)
          ";
          $rdate=$this->db->query($qdate)->result_array();

          $arrDate = array();
          $currDate = $dateFrom;
          while($currDate <= $dateTo) {
            $arrDate[] = $currDate;
            $currDate = date('Y-m-d', strtotime($currDate . " +1 days"));
          }

          foreach($arrDate as $d) {
            $objChartDate['labels'][] = date('d-m-y', strtotime($d));
            $searchIdx = array_search($d, array_column($rdate, 'Timestamp'));
            if($searchIdx!==false) {
              $objChartDate['datasets'][0]['data'][] = $rdate[$searchIdx]['COUNT'];
            } else {
              $objChartDate['datasets'][0]['data'][] = 0;
            }
          }
        } else {
          $arrMonth = array(
            1=>'JAN',
            2=>'FEB',
            3=>'MAR',
            4=>'APR',
            5=>'MEI',
            6=>'JUN',
            7=>'JUL',
            8=>'AGT',
            9=>'SEPT',
            10=>'OKT',
            11=>'NOP',
            12=>'DES',
          );
          $arrMonthCount = array();

          $qdate=@"
          select
          MONTH(TimeStamp) as Timestamp,
          COUNT(*) as COUNT
          from skm_tsurvey
          where
            $condOPD
          	and CAST(skm_tsurvey.Timestamp as DATE) >= '$dateFrom'
          	and CAST(skm_tsurvey.Timestamp as DATE) <= '$dateTo'
          group by MONTH(TimeStamp)
          order by MONTH(TimeStamp)
          ";
          $rdate=$this->db->query($qdate)->result_array();

          for($i=(int)date('m', strtotime($dateFrom)); $i<=(int)date('m', strtotime($dateTo)); $i++) {
            $objChartDate['labels'][] = $arrMonth[$i];
            $searchIdx = array_search($i, array_column($rdate, 'Timestamp'));
            if($searchIdx!==false) {
              $objChartDate['datasets'][0]['data'][] = $rdate[$searchIdx]['COUNT'];
            } else {
              $objChartDate['datasets'][0]['data'][] = 0;
            }
          }
        }
        ?>
        <div class="col-sm-12">
          <div class="card card-default">
            <div class="card-header text-center">
              <h5 class="card-title font-weight-bold" style="float: none">STATISTIK SURVEI KEPUASAN MASYARAKAT</h5>
              <p class="text-center mb-0">
                <strong><?=!empty($ropd)?strtoupper($ropd[COL_NMOPD]):'PEMERINTAH KABUPATEN BATU BARA'?></strong><br />
                <strong><?=date('d-m-Y', strtotime($dateFrom))?></strong> s.d <strong><?=date('d-m-Y', strtotime($dateTo))?></strong>
              </p>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-3">
                  <div class="card card-info">
                    <div class="card-header">
                      <h3 class="card-title">JENIS KELAMIN</h3>
                    </div>
                    <div class="card-body">
                      <canvas id="chartGender" style="height:30vh"></canvas>
                    </div>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="card card-info">
                    <div class="card-header">
                      <h3 class="card-title">UMUR</h3>
                    </div>
                    <div class="card-body">
                      <canvas id="chartUmur" style="height:30vh"></canvas>
                    </div>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="card card-info">
                    <div class="card-header">
                      <h3 class="card-title">PENDIDIKAN</h3>
                    </div>
                    <div class="card-body">
                      <canvas id="chartPendidikan" style="height:30vh"></canvas>
                    </div>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="card card-info">
                    <div class="card-header">
                      <h3 class="card-title">JENIS PEKERJAAN</h3>
                    </div>
                    <div class="card-body">
                      <canvas id="chartPekerjaan" style="height:30vh"></canvas>
                    </div>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="card card-info">
                    <div class="card-header">
                      <h3 class="card-title">JUMLAH RESPONDEN</h3>
                    </div>
                    <div class="card-body">
                      <canvas id="chartDate" style="height:30vh"></canvas>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function() {
  var pieCanvasGender = $('#chartGender').get(0).getContext('2d');
  var pieCanvasUmur = $('#chartUmur').get(0).getContext('2d');
  var pieCanvasPendidikan = $('#chartPendidikan').get(0).getContext('2d');
  var pieCanvasPekerjaan = $('#chartPekerjaan').get(0).getContext('2d');
  var chartCanvasDate = $('#chartDate').get(0).getContext('2d');

  var pieGender = new Chart(pieCanvasGender, {
    type: 'pie',
    data: <?=json_encode($objPieGender)?>,
    options: {
      maintainAspectRatio : true,
      responsive : true,
      plugins: {
        legend: {
          position: 'right',
          align: 'start'
        }
      }
    }
  });
  var pieUmur = new Chart(pieCanvasUmur, {
    type: 'pie',
    data: <?=json_encode($objPieUmur)?>,
    options: {
      maintainAspectRatio : true,
      responsive : true,
      plugins: {
        legend: {
          position: 'right',
          align: 'start'
        }
      }
    }
  });
  var piePendidikan = new Chart(pieCanvasPendidikan, {
    type: 'pie',
    data: <?=json_encode($objPiePendidikan)?>,
    options: {
      maintainAspectRatio : true,
      responsive : true,
      plugins: {
        legend: {
          position: 'right',
          align: 'start'
        }
      }
    }
  });
  var piePekerjaan = new Chart(pieCanvasPekerjaan, {
    type: 'pie',
    data: <?=json_encode($objPiePekerjaan)?>,
    options: {
      maintainAspectRatio : true,
      responsive : true,
      plugins: {
        legend: {
          position: 'right',
          align: 'start'
        }
      }
    }
  });

  var chartDate = new Chart(chartCanvasDate, {
    type: 'line',
    data: <?=json_encode($objChartDate)?>,
    options: {
      responsive : true
    }
  });
});
</script>
