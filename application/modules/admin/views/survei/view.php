<style>
.table-narrow td, .table-narrow th {
  padding: .3rem;
}
</style>
<div class="modal-header">
  <h5 class="modal-title">Detail Survei</h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true"><i class="fa fa-close"></i></span>
  </button>
</div>
<div class="modal-body">
  <div class="row">
    <div class="col-sm-12">
      <table class="table table-striped table-narrow elevation-1 text-sm">
        <tbody>
          <tr>
            <td style="width: 5px" class="nowrap">OPD</td>
            <td style="width: 5px">:</td>
            <td class="font-weight-bold"><?=$data[COL_NMOPD]?></td>

            <td style="width: 5px" class="nowrap">UMUR</td>
            <td style="width: 5px">:</td>
            <td class="font-weight-bold"><?=$data[COL_UMUR]?></td>
          </tr>
          <tr>
            <td style="width: 5px" class="nowrap">LAYANAN</td>
            <td style="width: 5px">:</td>
            <td class="font-weight-bold"><?=$data[COL_NMLAYANAN]?></td>

            <td style="width: 5px" class="nowrap">JENIS KELAMIN</td>
            <td style="width: 5px">:</td>
            <td class="font-weight-bold"><?=$data[COL_NMJENISKELAMIN]?></td>
          </tr>
          <tr>
            <td style="width: 5px" class="nowrap">WAKTU</td>
            <td style="width: 5px">:</td>
            <td class="font-weight-bold"><?=date('d-m-Y', strtotime($data[COL_TIMESTAMP]))?> <?=date('H:i', strtotime($data[COL_TIMESTAMP]))?></td>

            <td style="width: 5px" class="nowrap">PENDIDIKAN</td>
            <td style="width: 5px">:</td>
            <td class="font-weight-bold"><?=$data[COL_NMPENDIDIKAN]?></td>
          </tr>
          <tr>
            <td style="width: 5px" class="nowrap">NO. BUKU TAMU</td>
            <td style="width: 5px">:</td>
            <td class="font-weight-bold"><?=!empty($data[COL_NMREFERENSI])?$data[COL_NMREFERENSI]:'-'?></td>

            <td style="width: 5px" class="nowrap"> PEKERJAAN</td>
            <td style="width: 5px">:</td>
            <td class="font-weight-bold"><?=$data[COL_NMPEKERJAAN]?></td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="col-sm-12">
      <table class="table table-bordered table-condensed elevation-1 text-sm">
        <thead>
          <tr>
            <th style="padding: .3rem">UNSUR</th>
            <th style="padding: .3rem">JAWABAN</th>
            <th style="padding: .3rem" class="text-center">SKOR</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $sumskor = 0;
          foreach($skor as $s) {
            $sumskor += $s[COL_SKOR];
            ?>
            <tr>
              <td style="padding: .3rem"><?=$s[COL_NMKUISIONERJUDUL]?></td>
              <td style="padding: .3rem"><?=$s[COL_NMOPT]?></td>
              <td style="padding: .3rem" class="text-center font-weight-bold"><?=number_format($s[COL_SKOR])?></td>
            </tr>
            <?php
          }
          ?>
        </tbody>
        <thead>
          <tr>
            <th style="padding: .3rem" class="text-center" colspan="2">RATA-RATA</th>
            <th style="padding: .3rem" class="text-center"><?=number_format($sumskor/count($skor),2)?></th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
</div>
<?php
if(!empty($data[COL_NMSARAN])) {
  ?>
  <div class="modal-footer">
    <div class="col-sm-12">
      <div class="callout callout-info text-sm" style="margin-bottom: 0 !important">
        <p class="font-weight-bold mb-0">SARAN / MASUKAN :</p>
        <p><?=$data[COL_NMSARAN]?></p>
      </div>
    </div>
  </div>
  <?php
}
?>
