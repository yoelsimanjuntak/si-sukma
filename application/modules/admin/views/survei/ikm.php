<?php
$ruser = GetLoggedUser();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 font-weight-light"><?= $title ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-tachometer-alt"></i> DASHBOARD</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
          <div class="card card-default">
            <div class="card-header">
              <?=form_open(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal','method'=>'get'))?>
              <div class="row">
                <div class="col-sm-12">
                  <?php
                  if($ruser[COL_ROLEID] == ROLEADMIN) {
                    ?>
                    <div class="form-group row">
                      <label class="control-label col-sm-2 text-left mb-0">OPD / INSTANSI</label>
                      <div class="col-sm-5">
                        <select class="form-control " name="idOPD" style="width: 100%">
                          <?=GetCombobox("SELECT * FROM skm_mopd WHERE IsAktif = 1 ORDER BY NmOPD", COL_IDOPD, COL_NMOPD, (!empty($_GET['idOPD'])?$_GET['idOPD']:null), true, false, '-- SEMUA --')?>
                        </select>
                      </div>
                    </div>
                    <?php
                  } else {
                    $ropd = $this->db
                    ->where(COL_IDOPD, $ruser[COL_IDUNIT])
                    ->get(TBL_SKM_MOPD)
                    ->row_array();
                    ?>
                    <div class="form-group row">
                      <label class="control-label col-sm-2 text-left mb-0">OPD / INSTANSI</label>
                      <div class="col-sm-5">
                        <input type="text" class="form-control" value="<?=!empty($ropd)?$ropd[COL_NMOPD]:'--'?>" disabled />
                        <input type="hidden" name="idOPD" value="<?=$ruser[COL_IDUNIT]?>" />
                      </div>
                    </div>

                    <?php
                  }
                  ?>
                  <div class="form-group row">
                    <label class="control-label col-sm-2 text-left mb-0">PERIODE</label>
                    <div class="col-sm-2">
                      <input type="text" class="form-control datepicker text-right" name="filterDateFrom" value="<?=!empty($_GET['filterDateFrom'])?$_GET['filterDateFrom']:date('Y-m-1')?>" />
                    </div>
                    <label class="control-label col-sm-1 mb-0 text-center">s.d</label>
                    <div class="col-sm-2">
                      <input type="text" class="form-control datepicker text-right" name="filterDateTo" value="<?=!empty($_GET['filterDateTo'])?$_GET['filterDateTo']:date('Y-m-d')?>" />
                    </div>
                  </div>
                  <div class="form-group row" style="margin: 0 -20px !important; border-top: 1px solid #dedede">
                    <div class="col-sm-12 pl-3 mt-3">
                      <button type="submit" class="btn btn-outline-primary" title="Lihat"><i class="fa fa-arrow-circle-right"></i> TAMPILKAN</button>
                    </div>
                  </div>
                </div>
              </div>
              <?=form_close()?>
            </div>
          </div>
      </div>
      <?php
      if(!empty($_GET)) {
        $idOPD = $ruser[COL_ROLEID] == ROLEADMIN ? $_GET['idOPD'] : $ruser[COL_IDUNIT];
        $dateFrom = $_GET['filterDateFrom'];
        $dateTo = $_GET['filterDateTo'];

        $ropd = $this->db
        ->where(COL_IDOPD, $idOPD)
        ->get(TBL_SKM_MOPD)
        ->row_array();

        if(!empty($idOPD)) {
          $this->db->where(TBL_SKM_TSURVEY.'.'.COL_IDOPD, $idOPD);
        }
        $rsurvey = $this->db
        ->where('CAST('.TBL_SKM_TSURVEY.'.'.COL_TIMESTAMP.' as DATE) >= ', $dateFrom)
        ->where('CAST('.TBL_SKM_TSURVEY.'.'.COL_TIMESTAMP.' as DATE) <= ', $dateTo)
        ->get(TBL_SKM_TSURVEY)
        ->result_array();

        if(!empty($idOPD)) {
          $this->db->where(TBL_SKM_TSURVEY.'.'.COL_IDOPD, $idOPD);
        }
        $rsurveyskor = $this->db
        ->select('AVG(Skor) as Skor')
        ->where('CAST('.TBL_SKM_TSURVEY.'.'.COL_TIMESTAMP.' as DATE) >= ', $dateFrom)
        ->where('CAST('.TBL_SKM_TSURVEY.'.'.COL_TIMESTAMP.' as DATE) <= ', $dateTo)
        ->join(TBL_SKM_TSURVEYSKOR.' skor','skor.'.COL_IDSURVEY." = ".TBL_SKM_TSURVEY.".".COL_IDSURVEY,"left")
        ->group_by('skor.'.COL_IDKUISIONER)
        ->get(TBL_SKM_TSURVEY)
        ->result_array();
        $totalNRRT = 0;
        foreach($rsurveyskor as $skor) {
          $totalNRRT += $skor[COL_SKOR]*0.111;
        }

        $condOPD = ' 1=1 ';
        if(!empty($idOPD)) {
          $condOPD = " sur.IdOPD = $idOPD ";
        }
        $qsummary = @"
        select
        skor.NmKuisionerJudul,
        AVG(skor.Skor) as Skor
        from
        	skm_tsurveyskor skor
        left join skm_tsurvey sur on sur.IdSurvey = skor.IdSurvey
        where
          $condOPD
        	and CAST(sur.Timestamp as DATE) >= '$dateFrom'
        	and CAST(sur.Timestamp as DATE) <= '$dateTo'
        group by
        	skor.IdKuisioner, skor.NmKuisionerJudul
        order by
        	skor.IdKuisioner
        ";
        //echo $qsummary;
        //exit();
        $rsummary = $this->db
        ->query($qsummary)
        ->result_array();
        ?>
        <div class="col-sm-12">
            <div class="card card-default">
              <div class="card-header text-center">
                <h5 class="card-title font-weight-bold" style="float: none">PENGUKURAN INDEKS KEPUASAN MASYARAKAT</h5>
                <p class="text-center mb-0">
                  <strong><?=!empty($ropd)?strtoupper($ropd[COL_NMOPD]):'PEMERINTAH KABUPATEN BATU BARA'?></strong><br />
                  <strong><?=date('d-m-Y', strtotime($dateFrom))?></strong> s.d <strong><?=date('d-m-Y', strtotime($dateTo))?></strong>
                </p>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="info-box bg-info">
                      <span class="info-box-icon"><i class="far fa-star"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">IKM</span>
                        <span class="info-box-number"><?=number_format($totalNRRT*25, 2)?></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="info-box bg-primary">
                      <span class="info-box-icon"><i class="far fa-bookmark"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">MUTU LAYANAN</span>
                        <span class="info-box-number"><?=getSkorMutu($totalNRRT*25)?></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="info-box bg-danger">
                      <span class="info-box-icon"><i class="far fa-users"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">RESPONDEN</span>
                        <span class="info-box-number"><?=number_format(count($rsurvey))?></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <h6>SKOR PER UNSUR PELAYANAN</h6>
                    <table class="table table-striped elevation-1">
                      <thead>
                        <tr>
                          <th>No.</th>
                          <th>UNSUR PELAYANAN</th>
                          <th>NRR</th>
                          <th>NRR TERTIMBANG</th>
                          <!--<th>MUTU</th>-->
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $no=1;
                        $sumSkor = 0;
                        $sumNRRT = 0;
                        if(!empty($rsummary)) {
                          foreach($rsummary as $sum) {
                            $sumSkor += $sum[COL_SKOR];
                            $sumNRRT += $sum[COL_SKOR]*0.111;
                            ?>
                            <tr>
                              <td class="text-right"><?=$no?></td>
                              <td><?=$sum[COL_NMKUISIONERJUDUL]?></td>
                              <td class="text-right font-weight-bold"><?=number_format($sum[COL_SKOR], 2)?></td>
                              <td class="text-right font-weight-bold"><?=number_format(($sum[COL_SKOR]*0.111), 6)?></td>
                              <!--<td class="font-weight-bold"><?=getSkorMutu($sum[COL_SKOR])?></td>-->
                            </tr>
                            <?php
                            $no++;
                          }
                        } else {
                          ?>
                          <tr>
                            <td colspan="4" class="text-center font-italic">(belum ada data)</td>
                          </tr>
                          <?php
                        }
                        ?>
                      </tbody>
                      <tfoot>
                        <tr>
                          <th colspan="3" class="text-right">JLH. NRR TERTIMBANG</th>
                          <th class="text-right"><?=number_format($sumNRRT, 6)?></th>
                        </tr>
                        <tr>
                          <th colspan="3" class="text-right">IKM</th>
                          <th class="text-right"><?=number_format($sumNRRT*25, 6)?></th>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                  <div class="col-sm-6">
                    <h6>KETERANGAN</h6>
                    <table class="table table-bordered elevation-1">
                      <thead>
                        <tr>
                          <th class="text-center">SKOR</th>
                          <th class="text-center">MUTU</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="text-center">88.31 - 100</td>
                          <td>A (SANGAT BAIK)</td>
                        </tr>
                        <tr>
                          <td class="text-center">76.61 - 88.30</td>
                          <td>B (BAIK)</td>
                        </tr>
                        <tr>
                          <td class="text-center">65.00 - 76.60</td>
                          <td>C (KURANG BAIK)</td>
                        </tr>
                        <tr>
                          <td class="text-center">25.00 - 64.99</td>
                          <td>D (TIDAK BAIK)</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</section>
