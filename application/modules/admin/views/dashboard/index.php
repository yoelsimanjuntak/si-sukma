<?php
$dateFrom = date('Y-m-01');
$dateTo = date('Y-m-d');

$ruser = GetLoggedUser();
$this->db
->where('CAST('.TBL_SKM_TSURVEY.'.'.COL_TIMESTAMP.' as DATE) >= ', $dateFrom)
->where('CAST('.TBL_SKM_TSURVEY.'.'.COL_TIMESTAMP.' as DATE) <= ', $dateTo);
if($ruser[COL_ROLEID] != ROLEADMIN) {
  $this->db->where(TBL_SKM_TSURVEY.'.'.COL_IDOPD, $ruser[COL_IDUNIT]);
}
$rsurvey = $this->db->get(TBL_SKM_TSURVEY)->result_array();

$this->db
#->select('AVG(Skor) as Skor')
->where('CAST('.TBL_SKM_TSURVEY.'.'.COL_TIMESTAMP.' as DATE) >= ', $dateFrom)
->where('CAST('.TBL_SKM_TSURVEY.'.'.COL_TIMESTAMP.' as DATE) <= ', $dateTo);
if($ruser[COL_ROLEID] != ROLEADMIN) {
  $this->db->where(TBL_SKM_TSURVEY.'.'.COL_IDOPD, $ruser[COL_IDUNIT]);
}
#$this->db->join(TBL_SKM_TSURVEYSKOR.' skor','skor.'.COL_IDSURVEY." = ".TBL_SKM_TSURVEY.".".COL_IDSURVEY,"left");
#$this->db->group_by('skor.'.COL_IDKUISIONER);
$rsurveyskor = $this->db
->get(TBL_SKM_TSURVEY)
->result_array();

//echo $this->db->last_query();
//exit();

$totalNRRT = 0;
/*foreach($rsurveyskor as $skor) {
  $rskor_ = $this->db
  ->select('AVG(Skor) as Skor')
  ->where(COL_IDSURVEY, $skor[COL_IDSURVEY])
  ->get(TBL_SKM_TSURVEYSKOR)
  ->row_array();
  if(!empty($rskor_)) {
    $totalNRRT += $rskor_[COL_SKOR]*0.111;
  }
}
echo $totalNRRT;
exit();*/
$rsummary = array();
if($ruser[COL_ROLEID]==ROLEADMIN) {
  $qsummary = @"
  select
  skor.NmKuisionerJudul,
  AVG(skor.Skor) as Skor
  from
    skm_tsurveyskor skor
  left join skm_tsurvey sur on sur.IdSurvey = skor.IdSurvey
  where
    CAST(sur.Timestamp as DATE) >= '$dateFrom'
    and CAST(sur.Timestamp as DATE) <= '$dateTo'
  group by
    skor.IdKuisioner,skor.NmKuisionerJudul
  order by
    skor.IdKuisioner
  ";
  $rsummary = $this->db
  ->query($qsummary)
  ->result_array();
  foreach($rsummary as $skor) {
    $totalNRRT += $skor[COL_SKOR]*0.111;
  }
}
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?= $title ?></h3>
      </div><!-- /.col -->
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <?php
      if($ruser[COL_ROLEID]==ROLEADMIN) {
        $ropd = $this->db->query("select * from skm_mopd where IsAktif=1")
        /*->where(COL_ISAKTIF, 1)
        ->get(TBL_SKM_MOPD)*/
        ->result_array();
        ?>
        <div class="col-lg-<?=$ruser[COL_ROLEID]==ROLEADMIN?3:4?> col-12">
          <div class="small-box bg-primary">
            <div class="inner">
              <h3><?=number_format(count($ropd))?></h3>

              <p>OPD AKTIF<br /><small class="font-weight-bold">Kab. Batu Bara</small></p>
            </div>
            <div class="icon">
              <i class="fa fa-building"></i>
            </div>
          </div>
        </div>
        <?php
      }

      if($ruser[COL_ROLEID]!=ROLEADMIN) {
        $this->db->where(TBL_SKM_MLAYANAN.'.'.COL_IDOPD, $ruser[COL_IDUNIT]);
      }
      $this->db->where(TBL_SKM_MLAYANAN.'.'.COL_ISAKTIF, 1);
      $rlayanan = $this->db->query("select * from skm_mlayanan")
      /*->get(TBL_SKM_MLAYANAN)*/
      ->result_array();
      ?>
      <div class="col-lg-<?=$ruser[COL_ROLEID]==ROLEADMIN?3:4?> col-12">
        <div class="small-box bg-info">
          <div class="inner">
            <h3><?=number_format(count($rlayanan))?></h3>

            <p>LAYANAN AKTIF<br /><small class="font-weight-bold"><?=$ruser[COL_ROLEID]==ROLEADMIN?'Pemerintah Kab. Batu Bara':$ruser[COL_NMOPD]?></small></p>
          </div>
          <div class="icon">
            <i class="fa fa-bookmark"></i>
          </div>
        </div>
      </div>

      <div class="col-lg-<?=$ruser[COL_ROLEID]==ROLEADMIN?3:4?> col-12">
        <div class="small-box bg-danger">
          <div class="inner">
            <h3><?=number_format(count($rsurvey))?></h3>

            <p>SURVEI MASUK TA. <?=date('Y')?><br /><small class="font-weight-bold font-italic"><?=date('01-m-Y')?> s.d <?=date('d-m-Y')?></small></p>
          </div>
          <div class="icon">
            <i class="fa fa-ballot-check"></i>
          </div>
        </div>
      </div>
      <div class="col-lg-<?=$ruser[COL_ROLEID]==ROLEADMIN?3:4?> col-12">
        <div class="small-box bg-success">
          <div class="inner">
            <h3><?=number_format($totalNRRT*25, 2)?></h3>

            <p>IKM TA. <?=date('Y')?><br /><small class="font-weight-bold font-italic"><?=date('01-m-Y')?> s.d <?=date('d-m-Y')?></small></p>
          </div>
          <div class="icon">
            <i class="fa fa-star"></i>
          </div>
        </div>
      </div>

      <?php
      if($ruser[COL_ROLEID]==ROLEADMIN) {
        /*$qsummaryopd = @"
        select
        opd.NmOPD,
        (select
          AVG(skor.Skor)
          from skm_tsurvey sur
          left join skm_tsurveyskor skor on skor.IdSurvey = sur.IdSurvey
          where
            sur.IdOPD = opd.IdOPD
            and CAST(sur.Timestamp as DATE) >= '$dateFrom'
          	and CAST(sur.Timestamp as DATE) <= '$dateTo'
        ) as Skor
        from
        	skm_mopd opd
        where
          opd.IsAktif = 1
        order by
        	opd.NmOPD
        ";*/
        $qsummaryopd = @"
        select
        opd.NmOPD,
        opd.IdOPD
        from
        	skm_mopd opd
        where
          opd.IsAktif = 1
        order by
        	opd.NmOPD
        ";
        $rsummaryopd = $this->db
        ->query($qsummaryopd)
        ->result_array();
        ?>
        <div class="col-lg-6">
          <table class="table table-striped bg-white elevation-1">
            <thead>
              <tr>
                <th>No.</th>
                <th>UNSUR PELAYANAN</th>
                <th>NRR</th>
                <th>NRR TERTIMBANG</th>
                <!--<th class="d-none d-sm-block">MUTU</th>-->
              </tr>
            </thead>
            <tbody>
              <?php
              $no=1;
              $sumSkor = 0;
              $sumNRRT = 0;
              foreach($rsummary as $sum) {
                ?>
                <tr>
                  <td class="text-right"><?=$no?></td>
                  <td><?=$sum[COL_NMKUISIONERJUDUL]?></td>
                  <td class="text-right font-weight-bold"><?=number_format($sum[COL_SKOR], 2)?></td>
                  <td class="text-right font-weight-bold"><?=number_format($sum[COL_SKOR]*0.111, 6)?></td>
                  <!--<td class="font-weight-bold d-none d-sm-block"><?=getSkorMutu($sum[COL_SKOR])?></td>-->
                </tr>
                <?php
                $no++;
                $sumNRRT += $sum[COL_SKOR]*0.111;
              }
              ?>
            </tbody>
            <tfoot>
              <tr>
                <th colspan="3" class="text-right">JLH. NRR TERTIMBANG</th>
                <th class="text-right"><?=number_format($sumNRRT, 6)?></th>
              </tr>
              <tr>
                <th colspan="3" class="text-right">IKM</th>
                <th class="text-right"><?=number_format($sumNRRT*25, 6)?></th>
              </tr>
            </tfoot>
          </table>
        </div>
        <div class="col-lg-6">
          <div class="bg-white mb-3 p-3">
            <table id="tbl-opd" class="table table-striped elevation-1">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>OPD</th>
                  <th>SKOR</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $no=1;
                $sumSkor = 0;
                foreach($rsummaryopd as $sum) {
                  //$sumSkor += $sum[COL_SKOR];
                  ?>
                  <tr>
                    <td class="text-right"><?=$no?></td>
                    <td><?=$sum[COL_NMOPD]?></td>
                    <td class="text-right font-weight-bold col-skor-opd" data-url="<?=site_url('admin/ajax/get-skor-opd/'.$sum[COL_IDOPD])?>"><?=!empty($sum[COL_SKOR])?number_format($sum[COL_SKOR], 2):'<i class="far fa-spin fa-spinner"></i>'?></td>
                  </tr>
                  <?php
                  $no++;
                }
                ?>
              </tbody>
              <!--<tfoot>
                <tr>
                  <th colspan="2" class="text-right">RATA-RATA</th>
                  <th class="text-right"><?=count($rsummary)>0?number_format($sumSkor/count($rsummary), 2):'-'?></th>
                </tr>
              </tfoot>-->
            </table>
          </div>
        </div>
        <?php
      } else {
        $idOPD = $ruser[COL_IDUNIT];
        $qsummary = @"
        select
        skor.NmKuisionerJudul,
        AVG(skor.Skor) as Skor
        from
        	skm_tsurveyskor skor
        left join skm_tsurvey sur on sur.IdSurvey = skor.IdSurvey
        where
          sur.idOPD = $idOPD
          and CAST(sur.Timestamp as DATE) >= '$dateFrom'
        	and CAST(sur.Timestamp as DATE) <= '$dateTo'
        group by
        	skor.IdKuisioner,skor.NmKuisionerJudul
        order by
        	skor.IdKuisioner
        ";
        $rsummary = $this->db
        ->query($qsummary)
        ->result_array();

        $rlastsurvey = $this->db
        ->select('skm_tsurvey.*, '.TBL_SKM_MLAYANAN.'.NmLayanan, (select AVG(Skor) from skm_tsurveyskor skor where skor.IdSurvey = skm_tsurvey.IdSurvey) as Skor')
        ->where(TBL_SKM_TSURVEY.'.'.COL_IDOPD, $idOPD)
        ->join(TBL_SKM_MLAYANAN,TBL_SKM_MLAYANAN.'.'.COL_IDLAYANAN." = ".TBL_SKM_TSURVEY.".".COL_IDLAYANAN,"left")
        ->order_by(TBL_SKM_TSURVEY.".".COL_TIMESTAMP, 'desc')
        ->limit(10)
        ->get(TBL_SKM_TSURVEY)
        ->result_array();
        ?>
        <div class="col-lg-6">
          <table class="table table-striped bg-white elevation-1">
            <thead>
              <tr>
                <th>No.</th>
                <th>UNSUR PELAYANAN</th>
                <th>NRR</th>
                <th>NRR TERTIMBANG</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $no=1;
              $sumSkor = 0;
              $sumNRRT = 0;
              foreach($rsummary as $sum) {
                $sumSkor += $sum[COL_SKOR];
                $sumNRRT += $sum[COL_SKOR]*0.111;
                ?>
                <tr>
                  <td class="text-right"><?=$no?></td>
                  <td><?=$sum[COL_NMKUISIONERJUDUL]?></td>
                  <td class="text-right font-weight-bold"><?=number_format($sum[COL_SKOR], 2)?></td>
                  <td class="text-right font-weight-bold"><?=number_format($sumNRRT, 6)?></td>
                </tr>
                <?php
                $no++;
              }
              ?>
            </tbody>
            <tfoot>
              <tr>
                <th colspan="3" class="text-right">JLH. NRR TERTIMBANG</th>
                <th class="text-right"><?=number_format($sumNRRT, 6)?></th>
              </tr>
              <tr>
                <th colspan="3" class="text-right">IKM</th>
                <th class="text-right"><?=number_format($sumNRRT*25, 6)?></th>
              </tr>
            </tfoot>
            <!--<tfoot>
              <tr>
                <th colspan="2" class="text-right">RATA-RATA</th>
                <th class="text-right"><?=count($rsummary)>0?number_format($sumSkor/count($rsummary), 2):'-'?></th>
              </tr>
            </tfoot>-->
          </table>
        </div>
        <div class="col-lg-6">
          <table class="table table-striped bg-white elevation-1">
            <thead>
              <tr>
                <th>No.</th>
                <th>WAKTU / LAYANAN</th>
                <!--<th>LAYANAN</th>-->
                <th>SKOR</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $no=1;
              $sumSkor = 0;
              foreach($rlastsurvey as $sur) {
                $sumSkor += $sur[COL_SKOR];
                ?>
                <tr>
                  <td class="text-right"><?=$no?></td>
                  <td><?=date('d-m-Y H:i', strtotime($sur[COL_TIMESTAMP]))?><br /><small><?=$sur[COL_NMLAYANAN]?></small></td>
                  <!--<td></td>-->
                  <td class="text-right font-weight-bold"><?=number_format($sur[COL_SKOR], 2)?></td>
                </tr>
                <?php
                $no++;
              }
              ?>
            </tbody>
            <!--<tfoot>
              <tr>
                <th colspan="3" class="text-right">RATA-RATA</th>
                <th class="text-right"><?=count($rlastsurvey)>0?number_format($sumSkor/count($rlastsurvey), 2):'-'?></th>
              </tr>
            </tfoot>-->
          </table>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-data" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light">Form</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">

      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  var dtOPD = $('#tbl-opd').dataTable({
    "dom":"R<'row'<'col-sm-6' <'toolbar-dt-opd'>><'col-sm-6' f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "lengthChange": false,
    "bInfo" : false,
    "ordering": false,
    "pageLength": 5,
    "language": {
      "paginate": {
        "previous": "<i class='far fa-chevron-left'></i>",
        "next": "<i class='far fa-chevron-right'></i>"
      }
    }
  });
  $("div.toolbar-dt-opd").html('<h6 class="font-weight-bold" style="text-decoration: underline">REKAP IKM PER OPD</h6>');

  var elskor = $('td.col-skor-opd');
  if(elskor.length > 0) {
    for(var i=0; i<=elskor.length; i++) {
      $(elskor[i]).load($(elskor[i]).data('url'), function(){

      });
    }
  }
});
</script>
