<?php
class Master extends MY_Controller {
  function __construct() {
    parent::__construct();

    if(!IsLogin()) {
      redirect('site/home/login');
    }
  }

  public function opd_index() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN) {
      redirect('admin/dashboard');
    }

    $data['title'] = "OPD / Instansi Penyelenggara Layanan";
    $this->db->select('*,uc.Nm_FullName as Nm_CreatedBy, uu.Nm_FullName as Nm_UpdatedBy');
    $this->db->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_SKM_MOPD.".".COL_CREATEDBY,"left");
    $this->db->join(TBL__USERINFORMATION.' uu','uu.'.COL_USERNAME." = ".TBL_SKM_MOPD.".".COL_UPDATEDBY,"left");
    $this->db->order_by(COL_NMOPD, 'asc');
    $data['res'] = $this->db->get(TBL_SKM_MOPD)->result_array();
    //$this->load->view('user/index', $data);
    $this->template->load('main', 'admin/master/index-opd', $data);
  }

  public function opd_add() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN) {
      redirect('admin/dashboard');
    }

    if(!empty($_POST)) {
      $data = array(
        COL_NMOPD => $this->input->post(COL_NMOPD),
        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_SKM_MOPD, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function opd_edit($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN) {
      redirect('admin/dashboard');
    }

    if(!empty($_POST)) {
      $data = array(
        COL_NMOPD => $this->input->post(COL_NMOPD),
        COL_UPDATEDBY => $ruser[COL_USERNAME],
        COL_UPDATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_IDOPD, $id)->update(TBL_SKM_MOPD, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function opd_ubahstatus() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN) {
      redirect('admin/dashboard');
    }

    $data = $this->input->post('cekbox');
    $deleted = 0;
    $this->db->trans_begin();
    try {
      foreach ($data as $datum) {
        $rdata = $this->db
        ->where(COL_IDOPD, $datum)
        ->get(TBL_SKM_MOPD)
        ->row_array();
        if(empty($rdata)) {
          continue;
        }

        $res = $this->db
        ->where(COL_IDOPD, $datum)
        ->update(TBL_SKM_MOPD, array(
          COL_ISAKTIF=>$rdata[COL_ISAKTIF]==1?0:1
        ));
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }
      }
      $this->db->trans_commit();
      ShowJsonSuccess('OK');
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($e->getMessage());
      return;
    }
    if($deleted){
        ShowJsonSuccess($deleted." data diubah");
    }else{
        ShowJsonError("Tidak ada diubah");
    }
  }

  public function layanan_index() {
    $ruser = GetLoggedUser();
    $data['title'] = "Layanan";
    $this->db->select('skm_mlayanan.*, opd.NmOPD, uc.Nm_FullName as Nm_CreatedBy, uu.Nm_FullName as Nm_UpdatedBy');
    $this->db->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_SKM_MLAYANAN.".".COL_CREATEDBY,"left");
    $this->db->join(TBL__USERINFORMATION.' uu','uu.'.COL_USERNAME." = ".TBL_SKM_MLAYANAN.".".COL_UPDATEDBY,"left");
    $this->db->join(TBL_SKM_MOPD.' opd','opd.'.COL_IDOPD." = ".TBL_SKM_MLAYANAN.".".COL_IDOPD,"left");
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      $this->db->where(TBL_SKM_MLAYANAN.'.'.COL_IDOPD, $ruser[COL_IDUNIT]);
    }
    $this->db->order_by(COL_NMLAYANAN, 'asc');
    $data['res'] = $this->db->get(TBL_SKM_MLAYANAN)->result_array();
    //$this->load->view('user/index', $data);
    $this->template->load('main', 'admin/master/index-layanan', $data);
  }

  public function layanan_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_IDOPD => $this->input->post(COL_IDOPD),
        COL_NMLAYANAN => $this->input->post(COL_NMLAYANAN),
        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );
      if($ruser[COL_ROLEID]!=ROLEADMIN) {
        $data[COL_IDOPD] = $ruser[COL_IDUNIT];
      }

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_SKM_MLAYANAN, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function layanan_edit($id) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_IDOPD => $this->input->post(COL_IDOPD),
        COL_NMLAYANAN => $this->input->post(COL_NMLAYANAN),
        COL_UPDATEDBY => $ruser[COL_USERNAME],
        COL_UPDATEDON => date('Y-m-d H:i:s')
      );
      if($ruser[COL_ROLEID]!=ROLEADMIN) {
        $data[COL_IDOPD] = $ruser[COL_IDUNIT];
      }

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_IDLAYANAN, $id)->update(TBL_SKM_MLAYANAN, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function layanan_ubahstatus() {
    $ruser = GetLoggedUser();
    $data = $this->input->post('cekbox');
    $deleted = 0;
    $this->db->trans_begin();
    try {
      foreach ($data as $datum) {
        $rdata = $this->db
        ->where(COL_IDLAYANAN, $datum)
        ->get(TBL_SKM_MLAYANAN)
        ->row_array();
        if(empty($rdata)) {
          continue;
        }

        $res = $this->db
        ->where(COL_IDLAYANAN, $datum)
        ->update(TBL_SKM_MLAYANAN, array(
          COL_ISAKTIF=>$rdata[COL_ISAKTIF]==1?0:1
        ));
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }
      }
      $this->db->trans_commit();
      ShowJsonSuccess('OK');
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($e->getMessage());
      return;
    }
    if($deleted){
        ShowJsonSuccess($deleted." data diubah");
    }else{
        ShowJsonError("Tidak ada diubah");
    }
  }
}
