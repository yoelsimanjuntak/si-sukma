<?php
class Report extends MY_Controller {
  function __construct() {
    parent::__construct();

    if(!IsLogin()) {
      redirect('site/home/login');
    }
  }

  public function rekapitulasi() {
    $data['title'] = "Rekapitulasi Obat";
    $data['cetak'] = $cetak = $this->input->get("cetak");
    $month = $this->input->get("month") ? $this->input->get("month") : date('m');
    $year = $this->input->get("year") ? $this->input->get("year") : date('Y');

    if(!empty($month) && !empty($year)) {
      $dateFrom = $year.'-'.$month.'-01';
      $dateTo = date("Y-m-d", strtotime("+1 month", strtotime($dateFrom)));

      $q = @"
      select
r.IdStock,
r.NmSumber,
st.NmStock,
st.NmSatuan,
r.DateExpired,
(
	select coalesce(sum(r_.Jumlah),0)
	from tstockreceipt r_
	where
		r_.IdStock = r.IdStock
		and r_.DateReceipt < '$dateFrom'
) as PrevReceipt,
/*(
	select coalesce(sum(is_.Jumlah),0)
	from tstockdistribution_items is_
	inner join tstockdistribution dist_ on dist_.Uniq = is_.IdDistribution
	inner join tstockreceipt r_ on r_.Uniq = is_.IdReceipt
	where
		r_.IdStock = r.IdStock
		and dist_.DateDistribution < '$dateFrom'
) as PrevDistribution,*/
(
  select coalesce(sum(i_.Jumlah),0) as JlhIssue
  from tstockissue i_
  inner join tstockdistribution_items is_ on is_.Uniq = i_.IdItem
  inner join tstockdistribution dist_ on dist_.Uniq = is_.IdDistribution
  inner join tstockreceipt r_ on r_.Uniq = is_.IdReceipt
  where
    r_.IdStock = r.IdStock
    and i_.DateIssue < '$dateFrom'
) as PrevIssue,
(
	select coalesce(sum(r_.Jumlah),0)
	from tstockreceipt r_
	where
		r_.IdStock = r.IdStock
		and r_.DateReceipt >= '$dateFrom'
		and r_.DateReceipt < '$dateTo'
) as TotalReceipt
from tstockreceipt r
left join mstock st on st.IdStock = r.IdStock
group by r.IdStock
order by st.NmStock
      ";
      $data['res'] = $this->db->query($q)->result_array();
    }

    if($cetak) $this->load->view('admin/report/rekapitulasi_', $data);
    else $this->template->load('main', 'admin/report/rekapitulasi', $data);
  }

  public function stock_opname() {
    $data['title'] = "Stock Opname";
    $data['cetak'] = $cetak = $this->input->get("cetak");
    $month = $this->input->get("month") ? $this->input->get("month") : date('m');
    $year = $this->input->get("year") ? $this->input->get("year") : date('Y');

    if(!empty($month) && !empty($year)) {
      $dateFrom = $year.'-'.$month.'-01';
      $dateTo = date("Y-m-d", strtotime("+1 month", strtotime($dateFrom)));

      $q = @"
      select
r.IdStock,
r.NmSumber,
st.NmStock,
st.NmSatuan,
r.DateExpired,
(
	select coalesce(sum(r_.Jumlah),0)
	from tstockreceipt r_
	where
		r_.IdStock = r.IdStock
		and r_.DateReceipt < '$dateFrom'
) as PrevReceipt,
(
	select coalesce(sum(is_.Jumlah),0)
	from tstockdistribution_items is_
	inner join tstockdistribution dist_ on dist_.Uniq = is_.IdDistribution
	inner join tstockreceipt r_ on r_.Uniq = is_.IdReceipt
	where
		r_.IdStock = r.IdStock
		and dist_.DateDistribution < '$dateFrom'
) as PrevDistribution,
(
  select coalesce(sum(i_.Jumlah),0) as JlhIssue
  from tstockissue i_
  inner join tstockdistribution_items is_ on is_.Uniq = i_.IdItem
  inner join tstockdistribution dist_ on dist_.Uniq = is_.IdDistribution
  inner join tstockreceipt r_ on r_.Uniq = is_.IdReceipt
  where
    r_.IdStock = r.IdStock
    and i_.DateIssue < '$dateFrom'
) as PrevIssue,
(
	select coalesce(sum(r_.Jumlah),0)
	from tstockreceipt r_
	where
		r_.IdStock = r.IdStock
		and r_.DateReceipt >= '$dateFrom'
		and r_.DateReceipt < '$dateTo'
) as TotalReceipt,
(
	select coalesce(sum(is_.Jumlah),0)
	from tstockdistribution_items is_
	inner join tstockdistribution dist_ on dist_.Uniq = is_.IdDistribution
	inner join tstockreceipt r_ on r_.Uniq = is_.IdReceipt
	where
		r_.IdStock = r.IdStock
    and dist_.DateDistribution >= '$dateFrom'
		and dist_.DateDistribution < '$dateTo'
) as TotalDistribution
from tstockreceipt r
left join mstock st on st.IdStock = r.IdStock
group by r.IdStock
order by st.NmStock
      ";
      $data['res'] = $this->db->query($q)->result_array();
    }

    if($cetak) $this->load->view('admin/report/stockopname_', $data);
    else $this->template->load('main', 'admin/report/stockopname', $data);
  }

  public function stock_puskesmas() {
    $data['title'] = "Stock Puskesmas";
    $data['cetak'] = $cetak = $this->input->get("cetak");
    $month = $this->input->get("month") ? $this->input->get("month") : date('m');
    $year = $this->input->get("year") ? $this->input->get("year") : date('Y');
    $puskesmas = $this->input->get("idPuskesmas") ? $this->input->get("idPuskesmas") : null;

    if(!empty($month) && !empty($year) && !empty($puskesmas)) {
      $dateFrom = $year.'-'.$month.'-01';
      $dateTo = date("Y-m-d", strtotime("+1 month", strtotime($dateFrom)));
      $lastmonth = date('Y-m-d', strtotime($dateFrom.' -1 month'));

      $q = @"
      select
r.IdStock,
r.NmSumber,
st.NmStock,
st.NmSatuan,
r.DateExpired,
(
	select coalesce(sum(is_.Jumlah),0)
	from tstockdistribution_items is_
	inner join tstockdistribution dist_ on dist_.Uniq = is_.IdDistribution
	inner join tstockreceipt r_ on r_.Uniq = is_.IdReceipt
	where
		r_.IdStock = r.IdStock
		and dist_.DateDistribution < '$lastmonth'
    and dist_.IdPuskesmas = '$puskesmas'
) as PrevDistribution,
(
  select coalesce(sum(i_.Jumlah),0) as JlhIssue
  from tstockissue i_
  inner join tstockdistribution_items is_ on is_.Uniq = i_.IdItem
  inner join tstockdistribution dist_ on dist_.Uniq = is_.IdDistribution
  inner join tstockreceipt r_ on r_.Uniq = is_.IdReceipt
  where
    r_.IdStock = r.IdStock
    and i_.DateIssue < '$dateFrom'
    and dist_.IdPuskesmas = '$puskesmas'
) as PrevIssue,
(
	select coalesce(sum(is_.Jumlah),0)
	from tstockdistribution_items is_
	inner join tstockdistribution dist_ on dist_.Uniq = is_.IdDistribution
	inner join tstockreceipt r_ on r_.Uniq = is_.IdReceipt
	where
		r_.IdStock = r.IdStock
    and dist_.DateDistribution >= '$lastmonth'
		and dist_.DateDistribution < '$dateFrom'
    and dist_.IdPuskesmas = '$puskesmas'
) as CurrDistribution,
(
  select coalesce(sum(i_.Jumlah),0) as JlhIssue
  from tstockissue i_
  inner join tstockdistribution_items is_ on is_.Uniq = i_.IdItem
  inner join tstockdistribution dist_ on dist_.Uniq = is_.IdDistribution
  inner join tstockreceipt r_ on r_.Uniq = is_.IdReceipt
  where
    r_.IdStock = r.IdStock
    and dist_.DateDistribution >= '$lastmonth'
		and dist_.DateDistribution < '$dateFrom'
    and dist_.IdPuskesmas = '$puskesmas'
) as CurrIssue
from tstockreceipt r
left join mstock st on st.IdStock = r.IdStock
group by r.IdStock
order by st.NmStock
      ";
      $data['res'] = $this->db->query($q)->result_array();
    }

    if($cetak) $this->load->view('admin/report/stockpuskesmas_', $data);
    else $this->template->load('main', 'admin/report/stockpuskesmas', $data);
  }
}
?>
