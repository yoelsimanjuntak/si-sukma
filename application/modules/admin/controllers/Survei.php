<?php
class Survei extends MY_Controller {
  function __construct() {
    parent::__construct();

    if(!IsLogin()) {
      redirect('site/home/login');
    }
  }

  public function index(){
    $data['title'] = "Rekapitulasi Survei";
    $this->template->load('main', 'admin/survei/index', $data);
  }

  public function index_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $IdOPD = !empty($_POST['idOPD'])?$_POST['idOPD']:null;
    $dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    $dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_TIMESTAMP=>'desc');
    $orderables = array(null,COL_TIMESTAMP,COL_NMOPD,COL_NMLAYANAN);
    $cols = array(COL_TIMESTAMP,COL_NMOPD,COL_NMLAYANAN,COL_SKOR);

    $queryAll = $this->db
    ->join(TBL_SKM_MOPD.' opd','opd.'.COL_IDOPD." = ".TBL_SKM_TSURVEY.".".COL_IDOPD,"left")
    ->join(TBL_SKM_MLAYANAN.' lay','lay.'.COL_IDLAYANAN." = ".TBL_SKM_TSURVEY.".".COL_IDLAYANAN,"left")
    ->get(TBL_SKM_TSURVEY);

    $i = 0;
    foreach($cols as $item){
      if($item == COL_NMOPD) $item = 'opd.'.COL_NMOPD;
      if($item == COL_NMLAYANAN) $item = 'lay.'.COL_NMLAYANAN;
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($dateFrom)) {
      $this->db->where('CAST('.TBL_SKM_TSURVEY.'.'.COL_TIMESTAMP.' as DATE) >= ', $dateFrom);
    }
    if(!empty($dateTo)) {
      $this->db->where('CAST('.TBL_SKM_TSURVEY.'.'.COL_TIMESTAMP.' as DATE) <= ', $dateTo);
    }
    if(!empty($IdOPD)) {
      $this->db->where(TBL_SKM_TSURVEY.'.'.COL_IDOPD, $IdOPD);
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('skm_tsurvey.*, opd.NmOPD, lay.NmLayanan, 0 as Skor')
    ->join(TBL_SKM_MOPD.' opd','opd.'.COL_IDOPD." = ".TBL_SKM_TSURVEY.".".COL_IDOPD,"left")
    ->join(TBL_SKM_MLAYANAN.' lay','lay.'.COL_IDLAYANAN." = ".TBL_SKM_TSURVEY.".".COL_IDLAYANAN,"left")
    ->order_by(TBL_SKM_TSURVEY.".".COL_TIMESTAMP, 'desc')
    ->get_compiled_select(TBL_SKM_TSURVEY, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('admin/survei/view/'.$r[COL_IDSURVEY]).'" class="btn btn-xs btn-outline-info btn-view"><i class="fas fa-info-circle"></i></a>';
      $data[] = array(
        $htmlBtn,
        date('Y-m-d H:i', strtotime($r[COL_TIMESTAMP])),
        $r[COL_NMOPD],
        $r[COL_NMLAYANAN],
        '<span data-url="'.$r[COL_IDSURVEY].'"><i class="far fa-spin fa-spinner"></i></span>'
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function view($id) {
    $rdata = $this->db
    ->select('skm_tsurvey.*, opd.NmOPD, lay.NmLayanan')
    ->join(TBL_SKM_MOPD.' opd','opd.'.COL_IDOPD." = ".TBL_SKM_TSURVEY.".".COL_IDOPD,"left")
    ->join(TBL_SKM_MLAYANAN.' lay','lay.'.COL_IDLAYANAN." = ".TBL_SKM_TSURVEY.".".COL_IDLAYANAN,"left")
    ->where(COL_IDSURVEY, $id)
    ->get(TBL_SKM_TSURVEY)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }

    $arrdet = array();
    $rdet = $this->db
    ->select('skm_tsurveyskor.*')
    ->where(COL_IDSURVEY, $rdata[COL_IDSURVEY])
    ->order_by(COL_IDKUISIONER, 'asc')
    ->get(TBL_SKM_TSURVEYSKOR)
    ->result_array();
    $this->load->view('admin/survei/view', array('skor'=>$rdet,'data'=>$rdata));
  }

  public function ikm(){
    $data['title'] = "Indeks Kepuasan Masyarakat";
    $this->template->load('main', 'admin/survei/ikm', $data);
  }

  public function statistik(){
    $data['title'] = "Statistik";
    $this->template->load('main', 'admin/survei/statistik', $data);
  }
}
?>
