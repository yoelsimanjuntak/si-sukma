<?php
class Ajax extends MY_Controller {
  function now() {
    echo date('YmdHis');
  }

  public function changepassword() {
    $data['data'] = array();
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $this->form_validation->set_rules(array(
        array(
          'field' => 'NewPassword',
          'label' => 'NewPassword',
          'rules' => 'required|min_length[5]',
          'errors' => array('min_length' => 'Password minimal terdiri dari 5 karakter.')
        ),
        array(
          'field' => 'ConfirmPassword',
          'label' => 'Repeat ConfirmPassword',
          'rules' => 'required|matches[NewPassword]',
          'errors' => array('matches' => 'Kolom Konfirmasi Password wajib sama dengan Password Baru.')
        )
      ));

      if($ruser[COL_PASSWORD] != md5($this->input->post('OldPassword'))) {
        ShowJsonError('Password Lama tidak valid.');
        return false;
      }

      if($this->form_validation->run()) {
        $res = $this->db
        ->where(COL_USERNAME, $ruser[COL_USERNAME])
        ->update(TBL__USERS, array(COL_PASSWORD=>md5($this->input->post('NewPassword'))));
        if(!$res) {
          $err = $this->db->error();
          ShowJsonError($err['message']);
          return false;
        }

        ShowJsonSuccess('Password berhasil diperbarui.');
        return false;
      } else {
        ShowJsonError(validation_errors());
        return false;
      }
    } else {
      $this->load->view('admin/ajax/changepassword', $data);
    }
  }

  public function get_skor_opd($id) {
    $dateFrom = date('Y-m-01');
    $dateTo = date('Y-m-d');
    $qskor = @"
    select AVG(skor.Skor) as Skor
    from skm_tsurvey sur
    left join skm_tsurveyskor skor on skor.IdSurvey = sur.IdSurvey
    where
      sur.IdOPD = $id
      and CAST(sur.Timestamp as DATE) >= '$dateFrom'
      and CAST(sur.Timestamp as DATE) <= '$dateTo'
    ";
    $rskor = $this->db->query($qskor)->row_array();
    echo !empty($rskor)?number_format($rskor[COL_SKOR], 2):'-';
  }

  public function get_skor_s($id) {
    $qskor = @"
    select AVG(Skor) as Skor
    from skm_tsurveyskor skor
    where skor.IdSurvey = $id
    ";
    $rskor = $this->db->query($qskor)->row_array();
    echo !empty($rskor)?number_format($rskor[COL_SKOR], 2):'-';
  }
}
