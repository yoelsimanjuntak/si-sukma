<?php
class Dashboard extends MY_Controller {
  function __construct() {
    parent::__construct();

    if(!IsLogin()) {
      redirect('site/home/login');
    }

    if(IsLogin()) {
      $ruser = GetLoggedUser();
      if($ruser[COL_ROLEID] != ROLEADMIN) {
        //redirect('site/home');
      }
    }
  }

  public function index() {
    if(!IsLogin()) {
      //redirect('site/home/login');
    }

    $ruser = GetLoggedUser();
    /*if($ruser[COL_ROLEID] != ROLEADMIN) {
      //redirect('site/home');
    }*/

    $data['title'] = "Dashboard";
    $this->template->load('main', 'admin/dashboard/index', $data);
  }

  public function data_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];

    $ruser = GetLoggedUser();
    $orderdef = array(COL_TANGGAL=>'desc');
    $orderables = array(null,COL_NMJUDUL,COL_NMKATEGORI,null);
    $cols = array(COL_NMJUDUL,COL_TANGGAL,COL_NMKATEGORI);

    if($ruser[COL_ROLEID] != ROLEADMIN) {
      $this->db->where(TBL_TDATA.'.'.COL_IDKATEGORI, $ruser[COL_ROLEID]);
    }
    $queryAll = $this->db
    ->join(TBL_MKATEGORI.' k','k.'.COL_IDKATEGORI." = ".TBL_TDATA.".".COL_IDKATEGORI,"left")
    ->get(TBL_TDATA);

    $i = 0;
    foreach($cols as $item){
      if($item == COL_NMKATEGORI) $item = TBL_MKATEGORI.'.'.COL_NMKATEGORI;
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
        $order = $orderdef;
        $this->db->order_by(key($order), $order[key($order)]);
    }

    if($ruser[COL_ROLEID] != ROLEADMIN) {
      $this->db->where(TBL_TDATA.'.'.COL_IDKATEGORI, $ruser[COL_ROLEID]);
    }
    $q = $this->db
    ->join(TBL_MKATEGORI.' k','k.'.COL_IDKATEGORI." = ".TBL_TDATA.".".COL_IDKATEGORI,"left")
    ->get_compiled_select(TBL_TDATA, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    $colors = array('info', 'secondary', 'primary', 'warning', 'danger', 'success', 'dark');
    foreach($rec->result_array() as $r) {

      $htmlkeyword = '';
      if(!empty($r[COL_NMKEYWORD])) {
        $rkeyword_ = explode(',', $r[COL_NMKEYWORD]);
        foreach($rkeyword_ as $k) {
          $arrkeyword[] = $k;
          $htmlkeyword .= '<span class="badge badge-'.$colors[array_rand($colors)].' mr-1">'.$k.'</span>';
        }
      }

      $data[] = array(
        ($ruser[COL_ROLEID] == ROLEADMIN ? '<a href="'.site_url('admin/dashboard/data-delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-trash"></i></a>&nbsp;' : '').
        '<a href="'.MY_UPLOADURL.$r[COL_NMFILE].'" target="_blank" class="btn btn-xs btn-outline-'.(empty($r[COL_NMFILE])?'secondary':'primary').' '.(empty($r[COL_NMFILE])?'disabled':'').'"><i class="fas fa-download"></i></a>',
        date('Y-m-d', strtotime($r[COL_TANGGAL])),
        $r[COL_NMJUDUL],
        $htmlkeyword,
        empty($r[COL_NMKATEGORI])?'<span class="text-muted text-sm font-italic">belum terdisposisi</span>':$r[COL_NMKATEGORI],
        $r[COL_NMREMARKS]
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function data_add() {
    $ruser = GetLoggedUser();
    $selkategori = '';
    if(!empty($_POST)) {
      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
      $config['max_size'] = 102400;
      $config['max_width']  = 2560;
      $config['max_height']  = 2560;
      $config['overwrite'] = FALSE;

      $keyword_arr = $this->input->post('NmKeyword');
      $keyword = implode(',', $keyword_arr);
      $data = array(
        COL_NMJUDUL => $this->input->post(COL_NMJUDUL),
        COL_TANGGAL => $this->input->post(COL_TANGGAL),
        COL_NMKEYWORD => $keyword,
        COL_NMREMARKS => $this->input->post(COL_NMREMARKS),
        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      if(!empty($_FILES["userfile"]["name"])) {
        $this->load->library('upload',$config);
        if(!$this->upload->do_upload("userfile")){
            $err = $this->upload->display_errors();
            ShowJsonError(strip_tags($err));
            return;
        }

        $dataupload = $this->upload->data();
        if(!empty($dataupload) && $dataupload['file_name']) {
            $data[COL_NMFILE] = $dataupload['file_name'];
        }
      }

      /* perhitungan */
      try {
        $arrkey = array();
        $rkategori = $this->db->get(TBL_MKATEGORI)->result_array();
        $rdistinct = $this->db->distinct()->select(COL_NMKEYWORD)->get(TBL_MKATEGORI_DET)->num_rows();
        foreach($rkategori as $kat) {
          $rkeyword = $this->db
          ->where(COL_IDKATEGORI, $kat[COL_IDKATEGORI])
          ->get(TBL_MKATEGORI_DET)
          ->num_rows();

          foreach($keyword_arr as $key) {
            $rmatches = $this->db
            ->where(COL_IDKATEGORI, $kat[COL_IDKATEGORI])
            ->where(COL_NMKEYWORD, $key)
            ->get(TBL_MKATEGORI_DET)
            ->num_rows();

            $arrkey[$kat[COL_IDKATEGORI]] = (!empty($arrkey[$kat[COL_IDKATEGORI]])?$arrkey[$kat[COL_IDKATEGORI]]:0) + $rmatches+(1/($rdistinct + $rkeyword));
          }
        }

        $max = array_keys($arrkey,max($arrkey));
        if(!empty($max)) {
          $data[COL_IDKATEGORI] = $max[0];
          if(!empty($data[COL_IDKATEGORI])) {
            $rkat = $this->db
            ->where(COL_IDKATEGORI, $data[COL_IDKATEGORI])
            ->get(TBL_MKATEGORI)
            ->row_array();
            if(!empty($rkat)) {
              $selkategori = $rkat[COL_NMKATEGORI];
            }
          }
        }
      } catch(Exception $ex) {
        ShowJsonError($ex->getMessage());
        return;
      }
      /* perhitungan */

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TDATA, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Dokumen berhasil di disposisi ke kategori '.$selkategori);
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data = array();
      $this->load->view('admin/dashboard/form_data', $data);
    }
  }

  public function data_delete($id) {
    $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_TDATA);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      return;
    } else {
      ShowJsonSuccess('OK');
      return;
    }
  }

  public function member_add() {
    if(!empty($_POST)) {
      $this->form_validation->set_rules(array(
        array(
          'field' => COL_NM_EMAIL,
          'label' => COL_NM_EMAIL,
          'rules' => 'required|valid_email|is_unique[_userinformation.Nm_Email]',
          'errors' => array('is_unique' => 'Email sudah digunakan.')
        ),
        array(
          'field' => COL_PASSWORD,
          'label' => COL_PASSWORD,
          'rules' => 'required|min_length[5]',
          'errors' => array('min_length' => 'Password minimal terdiri dari 5 karakter.')
        )
      ));

      if(!$this->form_validation->run()) {
        $err = validation_errors();
        ShowJsonError($err);
        return false;
      }

      $userdata = array(
        COL_USERNAME => $this->input->post(COL_NM_EMAIL),
        COL_PASSWORD => md5($this->input->post(COL_PASSWORD)),
        COL_ROLEID => ROLEMEMBER
      );
      $userinfo = array(
        COL_USERNAME => $this->input->post(COL_NM_EMAIL),
        COL_NM_EMAIL => $this->input->post(COL_NM_EMAIL),
        COL_NM_FULLNAME => $this->input->post(COL_NM_FULLNAME),
        COL_NM_IDENTITYNO => $this->input->post(COL_NM_IDENTITYNO),
        COL_NM_GENDER => $this->input->post(COL_NM_GENDER),
        COL_NM_ADDRESS => $this->input->post(COL_NM_ADDRESS),
        COL_NM_PHONENO => $this->input->post(COL_NM_PHONENO),
        COL_DATE_REGISTERED => date('Y-m-d')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL__USERS, $userdata);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $res = $this->db->insert(TBL__USERINFORMATION, $userinfo);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($e->getMessage());
        return;
      }
    } else {
      $data = array();
      $this->load->view('admin/dashboard/_member', $data);
    }
  }

  public function member_edit($username) {
    $username = GetDecryption($username);
    if(!empty($_POST)) {
      $userinfo = array(
        COL_NM_FULLNAME => $this->input->post(COL_NM_FULLNAME),
        COL_NM_IDENTITYNO => $this->input->post(COL_NM_IDENTITYNO),
        COL_NM_GENDER => $this->input->post(COL_NM_GENDER),
        COL_NM_ADDRESS => $this->input->post(COL_NM_ADDRESS),
        COL_NM_PHONENO => $this->input->post(COL_NM_PHONENO)
      );

      $res = $this->db->where(COL_USERNAME, $username)->update(TBL__USERINFORMATION, $userinfo);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        return;
      }

      ShowJsonSuccess('OK');
      return;
    } else {
      $data['data'] = $rdata = $this->db->where(COL_USERNAME, $username)->get(TBL__USERINFORMATION)->row_array();
      if(empty($rdata)) {
        echo 'Data tidak valid.';
        return;
      }
      $this->load->view('admin/dashboard/_member', $data);
    }
  }

  public function member_delete($username) {
    $username = GetDecryption($username);

    $this->db->trans_begin();
    try {
      $res = $this->db->where(COL_USERNAME, $username)->delete(TBL__USERS);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $res = $this->db->where(COL_USERNAME, $username)->delete(TBL__USERINFORMATION);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $this->db->trans_commit();
      ShowJsonSuccess('OK');
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($e->getMessage());
      return;
    }
  }

  function kategori_index() {
    if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
        redirect('user/dashboard');
    }

    $data['title'] = "Kategori";
    $data['res'] = $this->db->get(TBL_MKATEGORI)->result_array();
    $this->template->load('main', 'admin/dashboard/index_kategori', $data);
  }

  public function kategori_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $keyword = $this->input->post(COL_NMKEYWORD);
      $arrkeyword = explode(',', $keyword);
      $det = array();
      $data = array(
        COL_NMKATEGORI => $this->input->post(COL_NMKATEGORI),
        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_MKATEGORI, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $idkat = $this->db->insert_id();
        if(count($arrkeyword)>0) {
          foreach($arrkeyword as $k) {
            $det[] = array(
              COL_IDKATEGORI=>$idkat,
              COL_NMKEYWORD=>trim($k),
              COL_CREATEDBY => $ruser[COL_USERNAME],
              COL_CREATEDON => date('Y-m-d H:i:s')
            );
          }
        }

        if(count($det)>0) {
          $res = $this->db->insert_batch(TBL_MKATEGORI_DET, $det);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data = array();
      $this->load->view('admin/dashboard/index_kategori', $data);
    }
  }

  public function kategori_edit($id) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $keyword = $this->input->post(COL_NMKEYWORD);
      $arrkeyword = explode(',', $keyword);
      $det = array();
      $data = array(
        COL_NMKATEGORI => $this->input->post(COL_NMKATEGORI),
        COL_UPDATEDBY => $ruser[COL_USERNAME],
        COL_UPDATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_IDKATEGORI, $id)->update(TBL_MKATEGORI, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $res = $this->db->where(COL_IDKATEGORI, $id)->delete(TBL_MKATEGORI_DET);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $idkat = $this->db->insert_id();
        if(count($arrkeyword)>0) {
          foreach($arrkeyword as $k) {
            $det[] = array(
              COL_IDKATEGORI=>$id,
              COL_NMKEYWORD=>trim($k),
              COL_CREATEDBY => $ruser[COL_USERNAME],
              COL_CREATEDON => date('Y-m-d H:i:s')
            );
          }
        }

        if(count($det)>0) {
          $res = $this->db->insert_batch(TBL_MKATEGORI_DET, $det);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data = array();
      $this->load->view('admin/dashboard/index_kategori', $data);
    }
  }

  public function kategori_delete() {
    $data = $this->input->post('cekbox');
    $deleted = 0;
    foreach ($data as $datum) {
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_IDKATEGORI, $datum)->delete(TBL_MKATEGORI);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $res = $this->db->where(COL_IDKATEGORI, $datum)->delete(TBL_MKATEGORI_DET);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($e->getMessage());
        return;
      }
    }
    if($deleted){
        ShowJsonSuccess($deleted." data dihapus");
    }else{
        ShowJsonError("Tidak ada dihapus");
    }
  }

  public function inisiasi_stock_awal() {
    $rstockawal = $this->db
    ->order_by('ID_PUSKESMAS')
    ->get("tstockawal")
    ->result_array();
    if(!empty($rstockawal)) {
      $html = @'<table border="1" style="white-space: nowrap">
      <tr>
      <td>#</td>
      <td>MSG</td>
      <td>ID</td>
      <td>NAMA_OBAT</td>
      <td>NAMA_PABRIK</td>
      <td>NAMA_SUPPLIER</td>
      <td>TGL_TERIMA</td>
      <td>BATCH</td>
      <td>PUSKESMAS</td>
      <td>NAMA_SUMBER</td>
      <td>HARGA</td>
      <td>JUMLAH</td>
      <td>TA</td>
      <td>TGL_EXPIRED</td>
      <td>ID_STOCK</td>
      <td>ID_PABRIK</td>
      <td>ID_SUPPLIER</td>
      <td>ID_PUSKESMAS</td>
      </tr>
      %s
      </table>';
      $row = '';
      $isOK = true;

      foreach($rstockawal as $st) {
        $msg = '';
        try {
          if(empty($st['TGL_TERIMA'])) {
            throw new Exception('TGL_TERIMA');
          }
          $d = DateTime::createFromFormat('Y-m-d', str_replace('/','-',$st['TGL_TERIMA']));
          if($d && $d->format('Y-m-d') === str_replace('/','-',$st['TGL_TERIMA'])) {

          } else {
            throw new Exception('TGL_EXPIRED');
          }

          $validate = date('Y-m-d', strtotime($st['TGL_TERIMA']));
        } catch(Exception $e) {
          $msg = $e->getMessage();
        }

        try {
          if(empty($st['TGL_EXPIRED'])) {
            throw new Exception('TGL_EXPIRED');
          }
          $d = DateTime::createFromFormat('Y-m-d', str_replace('/','-',$st['TGL_EXPIRED']));
          if($d && $d->format('Y-m-d') === str_replace('/','-',$st['TGL_EXPIRED'])) {

          } else {
            throw new Exception('TGL_EXPIRED');
          }

          $validate = date('Y-m-d', strtotime($st['TGL_EXPIRED']));
        } catch(Exception $e) {
          $msg = $e->getMessage();
          $isOK = false;
        }

        $temp = @'
        <tr '.(!empty($msg)?'style="background: red !important; font-weight: bold !important"':'').'>
        <td>'.(!empty($msg)?'ERROR':'OK').'</td>
        <td>'.(!empty($msg)?$msg:'-').'</td>
        <td>'.$st['id'].'</td>
        <td>'.$st['NAMA_OBAT'].'</td>
        <td>'.$st['NAMA_PABRIK'].'</td>
        <td>'.$st['NAMA_SUPPLIER'].'</td>
        <td>'.$st['TGL_TERIMA'].'</td>
        <td>'.$st['BATCH'].'</td>
        <td>'.$st['PUSKESMAS'].'</td>
        <td>'.$st['NAMA_SUMBER'].'</td>
        <td>'.$st['HARGA'].'</td>
        <td>'.$st['JUMLAH'].'</td>
        <td>'.$st['TA'].'</td>
        <td>'.$st['TGL_EXPIRED'].'</td>
        <td>'.$st['ID_STOCK'].'</td>
        <td>'.$st['ID_PABRIK'].'</td>
        <td>'.$st['ID_SUPPLIER'].'</td>
        <td>'.$st['ID_PUSKESMAS'].'</td>
        </tr>';
        $row .= $temp;
      }

      $html = sprintf($html, $row);
      echo $html;

      if($isOK) {
        $this->db->trans_begin();
        try {
          $lastpuskesmas = 0;
          foreach($rstockawal as $st) {
            $rreceipt = array(
              COL_IDSTOCK=>$st['ID_STOCK'],
              COL_IDPABRIK=>$st['ID_PABRIK'],
              COL_IDSUPPLIER=>$st['ID_SUPPLIER'],
              COL_DATERECEIPT=>date('Y-m-d', strtotime($st['TGL_TERIMA'])),
              COL_NMBATCH=>$st['BATCH'],
              COL_NMSUMBER=>$st['NAMA_SUMBER'],
              COL_HARGA=>toNum($st['HARGA']),
              COL_JUMLAH=>toNum($st['JUMLAH']),
              COL_TAHUN=>toNum($st['TA']),
              COL_DATEEXPIRED=>date('Y-m-d', strtotime($st['TGL_EXPIRED'])),
              COL_NMREMARKS=>'STOCK AWAL (SYSTEM)',
              COL_CREATEDBY=>'admin',
              COL_CREATEDON=>date('Y-m-d H:i:s')
            );
            $qreceipt = $this->db->insert(TBL_TSTOCKRECEIPT, $rreceipt);
            if(!$qreceipt) {
              throw new \Exception("INSERT RECEIPT FAIL AT ID:".$st['id']);
            }

            $idreceipt = $this->db->insert_id();

            if($lastpuskesmas != $st['ID_PUSKESMAS']) {
              $lastpuskesmas = $st['ID_PUSKESMAS'];
              $rdist = array(
                COL_IDPUSKESMAS=>$st['ID_PUSKESMAS'],
                COL_NMREFERENSI=>str_pad($st['ID_PUSKESMAS'], 4, "0", STR_PAD_LEFT),
                COL_DATEDISTRIBUTION=>date('Y-m-d', strtotime($st['TGL_TERIMA'])),
                COL_NMREMARKS=>'STOCK AWAL (SYSTEM)',
                COL_CREATEDBY=>'admin',
                COL_CREATEDON=>date('Y-m-d H:i:s')
              );
              $qdist = $this->db->insert(TBL_TSTOCKDISTRIBUTION, $rdist);
              if(!$qdist) {
                throw new \Exception("INSERT DIST FAIL AT ID:".$st['id']);
              }
              $iddist = $this->db->insert_id();
            }

            $rdistitems = array(
              COL_IDDISTRIBUTION=>$iddist,
              COL_IDRECEIPT=>$idreceipt,
              COL_JUMLAH=>toNum($st['JUMLAH'])
            );
            $qdistitems = $this->db->insert(TBL_TSTOCKDISTRIBUTION_ITEMS, $rdistitems);
            if(!$qdistitems) {
              throw new \Exception("INSERT DIST ITEMS FAIL AT ID:".$st['id']);
            }
          }

          $this->db->trans_commit();
          echo '<p>DB EXECUTION DONE..</p>';
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          echo '<p>'.$e->getMessage().'</p>';
        }
      }

      exit();
    } else {
      echo 'Tabel stock awal kosong.';
      exit();
    }
  }

  public function inisiasi_stock_awal_ifk() {
    $rstockawal = $this->db
    ->where('ID_PUSKESMAS', 0)
    ->order_by('ID_PUSKESMAS')
    ->get("tstockawal")
    ->result_array();
    if(!empty($rstockawal)) {
      $html = @'<table border="1" style="white-space: nowrap">
      <tr>
      <td>#</td>
      <td>MSG</td>
      <td>ID</td>
      <td>NAMA_OBAT</td>
      <td>NAMA_PABRIK</td>
      <td>NAMA_SUPPLIER</td>
      <td>TGL_TERIMA</td>
      <td>BATCH</td>
      <td>PUSKESMAS</td>
      <td>NAMA_SUMBER</td>
      <td>HARGA</td>
      <td>JUMLAH</td>
      <td>TA</td>
      <td>TGL_EXPIRED</td>
      <td>ID_STOCK</td>
      <td>ID_PABRIK</td>
      <td>ID_SUPPLIER</td>
      <td>ID_PUSKESMAS</td>
      </tr>
      %s
      </table>';
      $row = '';
      $isOK = true;

      foreach($rstockawal as $st) {
        $msg = '';
        try {
          if(empty($st['TGL_TERIMA'])) {
            throw new Exception('TGL_TERIMA');
          }
          $d = DateTime::createFromFormat('Y-m-d', str_replace('/','-',$st['TGL_TERIMA']));
          if($d && $d->format('Y-m-d') === str_replace('/','-',$st['TGL_TERIMA'])) {

          } else {
            throw new Exception('TGL_EXPIRED');
          }

          $validate = date('Y-m-d', strtotime($st['TGL_TERIMA']));
        } catch(Exception $e) {
          $msg = $e->getMessage();
        }

        try {
          if(empty($st['TGL_EXPIRED'])) {
            throw new Exception('TGL_EXPIRED');
          }
          $d = DateTime::createFromFormat('Y-m-d', str_replace('/','-',$st['TGL_EXPIRED']));
          if($d && $d->format('Y-m-d') === str_replace('/','-',$st['TGL_EXPIRED'])) {

          } else {
            throw new Exception('TGL_EXPIRED');
          }

          $validate = date('Y-m-d', strtotime($st['TGL_EXPIRED']));
        } catch(Exception $e) {
          $msg = $e->getMessage();
          $isOK = false;
        }

        $temp = @'
        <tr '.(!empty($msg)?'style="background: red !important; font-weight: bold !important"':'').'>
        <td>'.(!empty($msg)?'ERROR':'OK').'</td>
        <td>'.(!empty($msg)?$msg:'-').'</td>
        <td>'.$st['id'].'</td>
        <td>'.$st['NAMA_OBAT'].'</td>
        <td>'.$st['NAMA_PABRIK'].'</td>
        <td>'.$st['NAMA_SUPPLIER'].'</td>
        <td>'.$st['TGL_TERIMA'].'</td>
        <td>'.$st['BATCH'].'</td>
        <td>'.$st['PUSKESMAS'].'</td>
        <td>'.$st['NAMA_SUMBER'].'</td>
        <td>'.$st['HARGA'].'</td>
        <td>'.$st['JUMLAH'].'</td>
        <td>'.$st['TA'].'</td>
        <td>'.$st['TGL_EXPIRED'].'</td>
        <td>'.$st['ID_STOCK'].'</td>
        <td>'.$st['ID_PABRIK'].'</td>
        <td>'.$st['ID_SUPPLIER'].'</td>
        <td>'.$st['ID_PUSKESMAS'].'</td>
        </tr>';
        $row .= $temp;
      }

      $html = sprintf($html, $row);
      echo $html;

      if($isOK) {
        $this->db->trans_begin();
        try {
          $lastpuskesmas = 0;
          foreach($rstockawal as $st) {
            $rreceipt = array(
              COL_IDSTOCK=>$st['ID_STOCK'],
              COL_IDPABRIK=>$st['ID_PABRIK'],
              COL_IDSUPPLIER=>$st['ID_SUPPLIER'],
              COL_DATERECEIPT=>date('Y-m-d', strtotime($st['TGL_TERIMA'])),
              COL_NMBATCH=>$st['BATCH'],
              COL_NMSUMBER=>$st['NAMA_SUMBER'],
              COL_HARGA=>toNum($st['HARGA']),
              COL_JUMLAH=>toNum($st['JUMLAH']),
              COL_TAHUN=>toNum($st['TA']),
              COL_DATEEXPIRED=>date('Y-m-d', strtotime($st['TGL_EXPIRED'])),
              COL_NMREMARKS=>'STOCK AWAL (SYSTEM)',
              COL_CREATEDBY=>'admin',
              COL_CREATEDON=>date('Y-m-d H:i:s')
            );
            $qreceipt = $this->db->insert(TBL_TSTOCKRECEIPT, $rreceipt);
            if(!$qreceipt) {
              throw new \Exception("INSERT RECEIPT FAIL AT ID:".$st['id']);
            }

            $idreceipt = $this->db->insert_id();
          }

          $this->db->trans_commit();
          echo '<p>DB EXECUTION DONE..</p>';
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          echo '<p>'.$e->getMessage().'</p>';
        }
      }

      exit();
    } else {
      echo 'Tabel stock awal kosong.';
      exit();
    }
  }

  public function tambah_stock_awal() {
    $ruser = GetLoggedUser();
    $data['title'] = "Stok Awal Puskesmas";
    if(!empty($_POST)) {
      $puskesmas = $this->input->post("IdPuskesmas");
      $tanggal = $this->input->post("DateDistribution");
      $item = $this->input->post("DistItems");

      if(!empty($item)) {
        $this->db->trans_begin();
        $item = json_decode(urldecode($item));

        try {
          $row=1;
          $rdist = array(
            COL_IDPUSKESMAS=>$puskesmas,
            COL_NMREFERENSI=>str_pad($puskesmas, 4, "0", STR_PAD_LEFT),
            COL_DATEDISTRIBUTION=>date('Y-m-d', strtotime($tanggal)),
            COL_NMREMARKS=>'STOCK AWAL (USER)',
            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );
          $qdist = $this->db->insert(TBL_TSTOCKDISTRIBUTION, $rdist);
          if(!$qdist) {
            $err = $this->db->error();
            throw new \Exception("INSERT DIST FAIL AT ROW:".$row." (".$err['message'].")");
          }
          $iddist = $this->db->insert_id();

          foreach($item as $st) {
            $rreceipt = array(
              COL_IDSTOCK=>$st->IdStock,
              COL_IDPABRIK=>$st->IdPabrik,
              COL_IDSUPPLIER=>$st->IdSupplier,
              COL_DATERECEIPT=>date('Y-m-d', strtotime($tanggal)),
              COL_NMBATCH=>$st->NmBatch,
              COL_NMSUMBER=>$st->NmSumber,
              COL_HARGA=>toNum($st->Harga),
              COL_JUMLAH=>toNum($st->Jumlah),
              COL_TAHUN=>toNum($st->Tahun),
              COL_DATEEXPIRED=>date('Y-m-d', strtotime($tanggal)),
              COL_NMREMARKS=>'STOCK AWAL (USER)',
              COL_CREATEDBY=>$ruser[COL_USERNAME],
              COL_CREATEDON=>date('Y-m-d H:i:s')
            );
            $qreceipt = $this->db->insert(TBL_TSTOCKRECEIPT, $rreceipt);
            if(!$qreceipt) {
              throw new \Exception("INSERT RECEIPT FAIL AT ROW:".$row);
            }

            $idreceipt = $this->db->insert_id();
            $rdistitems = array(
              COL_IDDISTRIBUTION=>$iddist,
              COL_IDRECEIPT=>$idreceipt,
              COL_JUMLAH=>toNum($st->Jumlah)
            );
            $qdistitems = $this->db->insert(TBL_TSTOCKDISTRIBUTION_ITEMS, $rdistitems);
            if(!$qdistitems) {
              throw new \Exception("INSERT DIST ITEMS FAIL AT ROW:".$row);
            }
            $row++;
          }

          $this->db->trans_commit();
          ShowJsonSuccess('BERHASIL');
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
        }
      } else {
        ShowJsonError('DATA KOSONG!');
      }
    } else {
      $this->template->load('main', 'admin/dashboard/stockawal', $data);
    }
  }
}
